let statsAPIpitches = {

    pitcherPlaysMap: {},
    pitchersAvailable: {},
    pitcherNames: {},
    batterNames: {},
    teams: {},
    plateLength: 1.41666667,
    gamesAvailable: {},
    startDate: "",
    endDate: "",
    league: "",
    $loading: {},
    yPosHalf: 0,
    yPosAll: 0,
    yPosCur: 0,
    pitcherTotals: {},
    liveSelected: false,
    lastLiveAtBatIndex: -1,
    lastLivePitchIndex: 0,
    clearLive: true,
    playersJson: {},
    liveRunning: {},
    leaguesAvailable: {
        '1': {
            'name': 'Major League Baseball',
            'code': 'mlb',
            'abbreviation': 'MLB'
        },
        '11': {
            'name': 'Triple-A',
            'code': 'aaa',
            'abbreviation': 'MLB'
        },
        '12': {
            'name': 'Double-A',
            'code': 'aa',
            'abbreviation': 'AA'
        },
        '17': {
            'name': 'Offseason Leagues',
            'code': 'win',
            'abbreviation': 'WIN'
        },
    },

    init: function(){
        $(document)
            .ajaxStart(function () {
                statsAPIpitches.$loading.show();
            });
            /*
            .ajaxStop(function () {
                statsAPIpitches.$loading.hide();
            });
            */
        statsAPIpitches.$loading = $('#loadingDiv').hide();
        $('<div id="header" style="display:block;"></div>')
            .insertBefore('#loadingDiv');
        $('#header')
            .append('<div id="date-range-title" class="threeDpitch-font threeDpitch-title">Please select date range and league to search for games</div>')
            .append('<div id="date-range-input-parent" class="threeDpitch-font threeDpitch-selector-parent"></div>');
        statsAPIpitches.yPosHalf = $('#date-range-title').height();
        $('#date-range-input-parent')
            .append('<div id="league-select-div" class="threeDpitch-selector-div"><div class="threeDpitch-label threeDpitch-font">League</div><select id="league-select" class="threeDpitch-selector threeDpitch-shadowed"></select></div>')
            .append('<div class="threeDpitch-selector-div"><div class="threeDpitch-label threeDpitch-font">Start Date</div><input id="threeDpitch-start-date" class="threeDpitch-font threeDpitch-selector threeDpitch-shadowed"></div>')
            .append('<div class="threeDpitch-selector-div"><div class="threeDpitch-label threeDpitch-font">End Date</div><input id="threeDpitch-end-date" class="threeDpitch-font threeDpitch-selector threeDpitch-shadowed"></div>');
        $( "#threeDpitch-end-date" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            autoSize: true,
            maxDate: 0,
            minDate: "-2y",
            showOn: "focus",
            showAnim: "fold",
            onSelect: function(dateText) {
                statsAPIpitches.endDate = dateText;
                $("#threeDpitch-start-date").datepicker('option', 'maxDate', statsAPIpitches.endDate );
                //console.log("End Date: " + dateText);
                if ((statsAPIpitches.startDate != null && statsAPIpitches.startDate !== "") && (statsAPIpitches.league !== null && statsAPIpitches.league !== "")) {
                    statsAPIpitches.$loading.show();
                    statsAPIpitches.retrieveGames(statsAPIpitches.startDate, statsAPIpitches.endDate, statsAPIpitches.league);
                }
            },
            setDate: null
        });
        //$( "#threeDpitch-end-date" ).datepicker("setDate", 0);
        $( "#threeDpitch-start-date" ).datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true,
            changeYear: true,
            autoSize: true,
            maxDate: 0,
            minDate: "-2y",
            showOn: "focus",
            showAnim: "fold",
            onSelect: function(dateText) {
                statsAPIpitches.startDate = dateText;
                $("#threeDpitch-end-date").datepicker('option', 'minDate', statsAPIpitches.startDate );
                //console.log("Start Date: " + dateText);
                if ((statsAPIpitches.endDate != null && statsAPIpitches.endDate !== "") && (statsAPIpitches.league !== null && statsAPIpitches.league !== "")) {
                    statsAPIpitches.$loading.show();
                    statsAPIpitches.retrieveGames(statsAPIpitches.startDate, statsAPIpitches.endDate, statsAPIpitches.league);
                }
            },
            setDate: null
        });
        $.each(statsAPIpitches.leaguesAvailable, function (key, value) {
            $('#league-select')
                .append('<option value="' + key + '">' + value['name'] + '</option>');
        });
        $( "#league-select" )
            .prop('selectedIndex',-1)
            .change(function(){
                statsAPIpitches.league = $(this).val();
                //console.log("League selected: " + statsAPIpitches.league);
                if ((statsAPIpitches.endDate != null && statsAPIpitches.endDate !== "") && (statsAPIpitches.startDate != null && statsAPIpitches.startDate !== "")) {
                    statsAPIpitches.$loading.show();
                    statsAPIpitches.retrieveGames(statsAPIpitches.startDate, statsAPIpitches.endDate, statsAPIpitches.league);
                }
            });

        //$( "#threeDpitch-start-date" ).datepicker("setDate", "-1m");
    },

    reInit: function(){
        statsAPIpitches.$loading = $('#loadingDiv').show();
        Promise.all(
            [
                $('#header').fadeOut(function () {
                    $('#header').empty(); }).promise(),
                $('.threeDpitch-back-div').fadeOut(function () {
                    $('.threeDpitch-back-div').empty(); }).promise(),
                $('#containerDiv').fadeOut(function () {
                    $('#containerDiv').empty(); }).promise()
            ]
        ).then(async function () {
            let promise = new Promise((resolve, reject) => {
                resolve();
            });
            $('#header').remove();
            $('#threeDpitch-button-div').remove();
            $('.threeDpitch-back-div').remove();
            $('#containerDiv').remove();
            let response = await promise;
            //console.log('all done');
            statsAPIpitches.init();
        });
    },

    clearAll: function(){
        statsAPIpitches.pitcherPlaysMap = {};
        statsAPIpitches.pitchersAvailable = {};
        statsAPIpitches.pitcherNames = {};
        statsAPIpitches.batterNames = {};
        statsAPIpitches.gamesAvailable = {};
        statsAPIpitches.pitcherTotals = {};
        statsAPIpitches.startDate = "";
        statsAPIpitches.endDate = "";
        statsAPIpitches.league = "";
        statsAPIpitches.liveSelected = false;
        statsAPIpitches.playersJson = {};
        statsAPIpitches.clearLive = true;
    },

    clearTransient: function(){
        statsAPIpitches.pitcherTotals = {};
        statsAPIpitches.pitchersAvailable = {};
        statsAPIpitches.pitcherPlaysMap = {};
    },

    checkStorage: function(key, isObject){
        if(isObject){
            let object = sessionStorage.getItem(key);
            if(object !== null){
                return JSON.parse(object);
            } else {
                return null;
            }
        } else {
            let string = sessionStorage.getItem(key);
            if(string !== null){
                return string;
            } else {
                return null;
            }
        }
    },

    removeLive() {
        let containerDiv = $('#containerDiv');
        $('#matchupTitle')
            .fadeOut(function() {
                $('#matchupTitle').text("");
            });
        $('#pitcher-select-div')
            .fadeIn(function() {
                $('#pitcher-select').prop('selectedIndex',-1);
            });
        $('#batter-select-div')
            .fadeIn(function() {
                $('#batter-select').prop('selectedIndex',-1);
            });
        $('#inning-select-div')
            .fadeIn(function() {
                $('#inning-select').prop('selectedIndex',-1);
            });
        $('#live-select')
            .removeClass('ui-btn-active ui-state-persist threeDpitch-highlight-shadowed')
            .addClass('threeDpitch-light-shadowed')
            .blur();
        statsAPIpitches.liveSelected = false;
        statsAPIpitches.lastLiveAtBatIndex = -1;
        statsAPIpitches.lastLivePitchIndex = 0;
        if(containerDiv.length){
            containerDiv.fadeOut("fast");
        }
    },

    liveHandler: function(){
        let gameId = $('#game-select option:selected').val();
        //console.log("gameId: " + gameId);
        statsAPIpitches.retrieveLiveData(gameId);
    },

    retrieveLiveData: function (gameId){
        let liveDataObj = [];
        let curPlayEvents = {};
        $.ajax({
            url: "https://statsapi.mlb.com/api/v1.1/game/" + gameId + "/feed/live",
            method: 'GET',
        }).done(async function (data) {
            if(data.hasOwnProperty('metaData')){
                let metaData = data['metaData'];
                //console.log("metaData: ");
                //console.log(metaData);
                if(metaData !== null){
                    if(metaData['gameEvents'] !== null){
                        if(data.hasOwnProperty('liveData') && data.hasOwnProperty('gameData')) {
                            let liveData = data['liveData'];
                            //console.log("liveData: ");
                            //console.log(liveData);
                            let gameData = data['gameData'];
                            if ((liveData !== null && gameData !== null)) {
                                let promise = new Promise((resolve) => {
                                    resolve();
                                });
                                //console.log("gameData: ");
                                //console.log(gameData);
                                if (gameData.hasOwnProperty('players')) {
                                    let playersObj = statsAPIpitches.checkStorage(gameId + "_players", true);
                                    if (playersObj === null) {
                                        playersObj = gameData['players'];
                                        sessionStorage.setItem(gameId + "_players", JSON.stringify(playersObj));
                                        //console.log("No players object found in storage");
                                    } else {
                                        //console.log("Retrieved players object from storage");
                                    }
                                    statsAPIpitches.playersJson = playersObj;
                                    //console.log(playersObj);
                                }
                                if (liveData.hasOwnProperty('plays')) {
                                    let finalPlayEvents;
                                    if (liveData['plays'].hasOwnProperty('currentPlay')) {
                                        let currentPlay = liveData['plays']['currentPlay'];
                                        if (currentPlay !== null) {
                                            let atBatIndex = Number(currentPlay['about']['atBatIndex']);
                                            //console.log("CurrentAtBatIndex: " + atBatIndex);
                                            //console.log("LastLiveAtBatIndex: " + statsAPIpitches.lastLiveAtBatIndex);
                                            if (statsAPIpitches.lastLiveAtBatIndex === -1) {
                                                statsAPIpitches.clearLive = true;
                                            } else {
                                                statsAPIpitches.clearLive = statsAPIpitches.lastLiveAtBatIndex < atBatIndex;
                                            }
                                            //console.log("ClearLive: " + statsAPIpitches.clearLive);
                                            statsAPIpitches.lastLiveAtBatIndex = atBatIndex;
                                            liveDataObj.isComplete = currentPlay['about']['isComplete'];
                                            liveDataObj.inning = currentPlay['about']['inning'];
                                            let halfInning = currentPlay['about']['halfInning'];
                                            liveDataObj.halfInning = halfInning.charAt(0).toUpperCase() + halfInning.substr(1, 2);
                                            liveDataObj.outs = currentPlay['count']['outs'];
                                            liveDataObj.batterId = currentPlay['matchup']['batter']['id'];
                                            liveDataObj.batterName = currentPlay['matchup']['batter']['fullName'];
                                            liveDataObj.batterSide = currentPlay['matchup']['batSide']['code'];
                                            liveDataObj.pitcherId = currentPlay['matchup']['pitcher']['id'];
                                            liveDataObj.pitcherName = currentPlay['matchup']['pitcher']['fullName'];
                                            liveDataObj.pitcherHand = currentPlay['matchup']['pitchHand']['code'];
                                            liveDataObj.resultCode = currentPlay['result']['event'];
                                            liveDataObj.resultDesc = currentPlay['result']['description'];
                                            liveDataObj.menOnBase = currentPlay['matchup']['splits']['menOnBase'];
                                            liveDataObj.szTop = statsAPIpitches.playersJson['ID' + liveDataObj.batterId].strikeZoneTop;
                                            liveDataObj.szBot = statsAPIpitches.playersJson['ID' + liveDataObj.batterId].strikeZoneBottom;
                                            $('#matchupTitle')
                                                .text(liveDataObj.pitcherName + " vs " + liveDataObj.batterName)
                                                .fadeIn();
                                        }
                                        curPlayEvents = currentPlay['playEvents'];
                                        finalPlayEvents = {};
                                        let curEventsLength = curPlayEvents.length;
                                        let tempIndex = 0;
                                        let curIndex = 0;
                                        if (!statsAPIpitches.clearLive) {
                                            //console.log("lastLivePitchIndex: " + statsAPIpitches.lastLivePitchIndex);
                                            let tempEvents = [];
                                            for (let i = 0; i < curEventsLength; i++) {
                                                if (curPlayEvents[i]['isPitch']) {
                                                    curIndex = Number(curPlayEvents[i]['index']);
                                                    //console.log("curIndex: " + curIndex);
                                                    if (curIndex > statsAPIpitches.lastLivePitchIndex) {
                                                        tempEvents.push(curPlayEvents[i]);
                                                        tempIndex = Math.max(tempIndex, curIndex);
                                                    }
                                                }
                                            }
                                            //console.log("tempIndex: " + tempIndex);
                                            finalPlayEvents = tempEvents;
                                        } else {
                                            for (let j = 0; j < curEventsLength; j++) {
                                                if (curPlayEvents[j]['isPitch']) {
                                                    curIndex = Number(curPlayEvents[j]['index']);
                                                    //console.log("curIndex: " + curIndex);
                                                    if (curIndex > statsAPIpitches.lastLivePitchIndex) {
                                                        tempIndex = Math.max(tempIndex, curIndex);
                                                    }
                                                }
                                            }
                                            statsAPIpitches.lastLivePitchIndex = tempIndex;
                                            finalPlayEvents = currentPlay['playEvents'];
                                        }
                                        //console.log("curPlayEvents:");
                                        //console.log(curPlayEvents);
                                    }
                                    let eventsMap = statsAPIpitches.processPlayEvents(finalPlayEvents, liveDataObj.pitcherHand, liveDataObj.batterSide, liveDataObj.menOnBase, liveDataObj.outs, liveDataObj.resultCode, liveDataObj.resultDesc, liveDataObj.pitcherId, liveDataObj.batterId, liveDataObj.inning, liveDataObj.halfInning);
                                    //console.log(eventsMap);
                                    await promise;
                                    if ($('#containerDiv').length) {
                                        threeDpitches.plotPitches([eventsMap], false, false, true, liveDataObj.isComplete, statsAPIpitches.clearLive);
                                    } else {
                                        threeDpitches.plotPitches([eventsMap], true, false, true, liveDataObj.isComplete, statsAPIpitches.clearLive);
                                    }
                                } else {
                                    statsAPIpitches.noGameDataDialog();
                                }
                            } else {
                                statsAPIpitches.noGameDataDialog();
                            }
                        }
                    }
                }
            }
        }).fail(function (status) {
            statsAPIpitches.noGameDataDialog(status);
        });
    },

    retrieveGames: async function(startDate, endDate, league){
        let storedItem = statsAPIpitches.checkStorage(league + "_" + startDate + "_" + endDate + "_gamesAvailable", true);
        if(storedItem === null) {
            let promise = new Promise((resolve) => {
                resolve();
            });
            $('#header').fadeOut();
            await promise;
            let url = "http://statsapi.mlb.com/api/v1/schedule/games/?sportId=" + league + "&startDate=" + startDate + "&endDate=" + endDate + "";
            $.ajax({
                url: url,
                method: 'GET',
            }).done(function (data) {
                if (data.hasOwnProperty('dates')) {
                    let dates = data['dates'];
                    if (dates.length > 0) {
                        $('#header').empty();
                        for (let i = 0; i < dates.length; i++) {
                            let date = dates[i]['date'];
                            let games = dates[i]['games'];
                            for (let j = 0; j < games.length; j++) {
                                let gameArr = [];
                                let gameObj = {
                                    "gameId": games[j]['gamePk'],
                                    "awayTeam": games[j]['teams']['away']['team']['name'],
                                    "homeTeam": games[j]['teams']['home']['team']['name']
                                };
                                if (statsAPIpitches.gamesAvailable.hasOwnProperty(date)) {
                                    gameArr = statsAPIpitches.gamesAvailable[date];
                                    gameArr.push(gameObj);
                                    statsAPIpitches.gamesAvailable[date] = gameArr;
                                } else {
                                    gameArr.push(gameObj);
                                    statsAPIpitches.gamesAvailable[date] = gameArr;
                                }
                            }
                        }
                        //console.log("saving date range search and results");
                        sessionStorage.setItem(league + "_" + startDate + "_" + endDate + "_gamesAvailable", JSON.stringify(statsAPIpitches.gamesAvailable));
                        statsAPIpitches.rebuildHeader();
                    } else {
                        statsAPIpitches.noGamesDialog();
                    }

                } else {
                    statsAPIpitches.noGamesDialog();
                }
            }).fail(function (status) {
                statsAPIpitches.noGamesDialog(status);
            });
        } else {
            $('#header').fadeOut( async function(){
                $('#header').empty();
                //console.log("retrieving date range saved search and results");
                let promise = new Promise((resolve, reject) => {
                    resolve();
                });
                statsAPIpitches.gamesAvailable = storedItem;
                await promise;
                statsAPIpitches.rebuildHeader();
            });
        }
    },

    clearNoGamesAvailable: function() {
        $("#threeDpitch-end-date").datepicker("setDate", null);
        $("#threeDpitch-start-date").datepicker("setDate", null);
        $("#league-select").prop('selectedIndex',-1);
        statsAPIpitches.startDate = "";
        statsAPIpitches.endDate = "";
        statsAPIpitches.league = "";
    },

    noGamesDialog: function(status){
        let dialogDiv = $('<div id="dialog-message">Failure retrieving games: Error ' + status + ', please select again</div>');
        if(status === null || status === undefined){
            dialogDiv = $('<div id="dialog-message">No games for selected dates, please select again</div>');
        }
        statsAPIpitches.clearNoGamesAvailable();
        dialogDiv.dialog({
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $('#header').fadeIn("fast");
                }
            }
        });
        statsAPIpitches.$loading.hide();
    },

    liveClick: function() {
        statsAPIpitches.lastLiveAtBatIndex = -1;
        if(statsAPIpitches.liveSelected) {
            statsAPIpitches.removeLive();
        } else {
            statsAPIpitches.$loading.show();
            $('#pitcher-select-div')
                .fadeOut(function(){
                    $('#pitcher-select-div')
                        .prop('selectedIndex',-1);
                });
            $('#batter-select-div')
                .fadeOut(function(){
                    $('#batter-select-div')
                        .find('option')
                        .remove()
                        .end();
                });
            $('#inning-select-div')
                .fadeOut(function(){
                    $('#inning-select-div')
                        .find('option')
                        .remove()
                        .end();
                });
            $('#live-select')
                .removeClass('threeDpitch-light-shadowed')
                .addClass('ui-btn-active ui-state-persist threeDpitch-highlight-shadowed');
            statsAPIpitches.liveSelected = true;
            statsAPIpitches.liveHandler();
            //console.log("Starting Live Feed");
            statsAPIpitches.liveRunning = setInterval(function(){
                if(statsAPIpitches.liveSelected){
                    //console.log("Updating Pitches");
                    statsAPIpitches.liveHandler();
                } else {
                    //console.log("Ending Live Feed");
                    clearInterval(statsAPIpitches.liveRunning);
                }
            }, 10000);
        }
    },

    rebuildHeader: async function() {
        let headerDiv = $('#header'), containerDiv = $('#containerDiv');
        let promise = new Promise((resolve) => {
            resolve();
        });
        $('<div class="threeDpitch-back-div"><img id="date-range-return" class="threeDpitch-back-arrow threeDpitch-light-shadowed" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAMAAAANIilAAAAAxlBMVEUAAABpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWlpaWl+xKtIAAAAQXRSTlMAAQIDBAUGCAkLDQ4PGRwdICQzNzg8PUJKTE1OUFZcXV9iZHuAgoiJj56jqK2vsLW6vMrOz9XZ3ODk6evv8/n7/Vt3xbQAAADpSURBVEjH7dXJUsJAFIXhk5AQZoygjA44gQoyKDMq5/1fygUUJVTRwOkl+fffpuve20BU1BkXODJNvHLiadSp/5AsSDY7IElWBBp75Krq6fZyRhUn30kROze/VHHuixSx98zt2vfryv4hW5xzbyPXSNMdmsqbHupuabQM99uLISliv0mq+HpBFQddUsYdWuC+DU70LDBQ+rbAiLcsMBCOLDCchj6eADJdeTEA4MowaWP30EL7Lzvk42FdLX7ELdnZrlMP4O1Sx0CqbYH/3zIBw3uywEDuU/7oNj9HEWLJN059yAUuoqLOqj8TO7g70Ps42AAAAABJRU5ErkJggg=="><div class="threeDpitch-back-label threeDpitch-font">Back to date range</div></div>')
            .insertBefore('#loadingDiv');
        $('<div id="threeDpitch-button-div"><button id="live-select" class="threeDpitch-button ui-button ui-corner-all threeDpitch-light-shadowed">LIVE</button></div>')
            .insertBefore('#loadingDiv');
        $('#date-range-return')
            .mousedown(function(){
                $('#date-range-return').removeClass("threeDpitch-light-shadowed");
            })
            .mouseout(function(){
                $('#date-range-return').addClass("threeDpitch-light-shadowed");
            })
            .mouseup(function(){
                $('#date-range-return').addClass("threeDpitch-light-shadowed");
            })
            .click(function(){
                statsAPIpitches.liveSelected = false;
                $('#threeDpitch-button-div').fadeOut(function () {
                    statsAPIpitches.removeLive();
                });
                statsAPIpitches.clearAll();
                statsAPIpitches.reInit();
            });
        headerDiv.fadeIn();
        headerDiv
            .append('<div id="gameDate" class="threeDpitch-font threeDpitch-title"></div>')
            .append('<div id="gameTitle" class="threeDpitch-font threeDpitch-title"></div>')
            .append('<div id="matchupTitle" class="threeDpitch-font threeDpitch-secondary-title"></div>')
            .append('<div id="selector-parent" class="threeDpitch-font threeDpitch-selector-parent"></div>');
        $('#matchupTitle').hide();
        let gameTitle = $('#gameTitle'), selectorParent = $('#selector-parent');
        statsAPIpitches.yPosAll = gameTitle.height() + $('#gameDate').height();

        selectorParent
            .append('<div id="date-select-div" class="threeDpitch-selector-div"><div class="threeDpitch-label threeDpitch-font">Game Date</div><select id="date-select" class="threeDpitch-selector threeDpitch-shadowed"></select></div>')
            .append('<div id="game-select-div" class="threeDpitch-selector-div"><div class="threeDpitch-label threeDpitch-font">Game</div><select id="game-select" class="threeDpitch-selector threeDpitch-shadowed"></select></div>')
            .append('<div id="pitcher-select-div" class="threeDpitch-selector-div"><div class="threeDpitch-label threeDpitch-font">Pitcher</div><select id="pitcher-select" class="threeDpitch-selector threeDpitch-shadowed"></select></div>')
            .append('<div id="batter-select-div" class="threeDpitch-selector-div"><div class="threeDpitch-label threeDpitch-font">Batter</div><select id="batter-select" class="threeDpitch-selector threeDpitch-shadowed"></select></div>')
            .append('<div id="inning-select-div" class="threeDpitch-selector-div"><div class="threeDpitch-label threeDpitch-font">Inning</div><select id="inning-select" class="threeDpitch-selector threeDpitch-shadowed"></select></div>');
        $('#threeDpitch-button-div')
            .hide();
        let gameSelect = $('#game-select'), dateSelect = $("#date-select");
        $.each(statsAPIpitches.gamesAvailable, function (key) {
            $('#date-select')
                .append('<option value="' + key + '">' + key + '</option>');
        });
        dateSelect.prop('selectedIndex',-1);
        dateSelect
            .change(function() {
                if(containerDiv.length){
                    containerDiv.fadeOut("fast");
                }
                let dateId = $("#date-select option:selected").val();
                //console.log("selected date: " + dateId);
                $('#gameDate').text(dateId);
                statsAPIpitches.yPosCur = selectorParent.position().top;
                selectorParent.css('top', statsAPIpitches.yPosHalf);
                gameTitle.fadeOut(function(){
                    gameTitle.text("Please select a game");
                }).fadeIn();
                //$('#selector-parent').css('top', statsAPIpitches.yPosCur);
                gameSelect
                    .find('option')
                    .remove()
                    .end();
                $('#pitcher-select')
                    .find('option')
                    .remove()
                    .end();
                $('#batter-select')
                    .find('option')
                    .remove()
                    .end();
                $('#inning-select')
                    .find('option')
                    .remove()
                    .end();
                let gamesArr = statsAPIpitches.gamesAvailable[dateId];
                for(let i = 0; i < gamesArr.length; i++){
                    let away = gamesArr[i]['awayTeam'];
                    let home = gamesArr[i]['homeTeam'];
                    $('#game-select')
                        .append('<option value="' + gamesArr[i]['gameId'] + '" away="'+away+'" home="'+home+'">' + gamesArr[i]['gameId'] + ' - ' + gamesArr[i]['awayTeam'] +' @ ' + gamesArr[i]['homeTeam'] +'</option>');
                }
                if(dateSelect.prop('selectedIndex') !== -1 && statsAPIpitches.liveSelected){
                    statsAPIpitches.removeLive();
                }
                gameSelect.prop('selectedIndex',-1);
            });
        gameSelect.prop('selectedIndex',-1);
        gameSelect
            .change(function() {
                statsAPIpitches.$loading.show();
                if(containerDiv.length){
                    containerDiv.fadeOut("fast");
                }
                let selectGameOptions = $("#game-select option:selected");
                let gameId = selectGameOptions.val();
                let awayTeam = selectGameOptions.attr('away');
                let homeTeam = selectGameOptions.attr('home');
                $('#gameTitle').fadeOut(function(){
                    $('#gameTitle').text(awayTeam + " @ " + homeTeam);
                }).fadeIn();
                //console.log("selected game: " + gameId);
                $('#pitcher-select')
                    .find('option')
                    .remove()
                    .end();
                $('#batter-select')
                    .find('option')
                    .remove()
                    .end();
                $('#inning-select')
                    .find('option')
                    .remove()
                    .end();
                $('#threeDpitch-button-div').fadeIn();
                statsAPIpitches.lastLiveAtBatIndex = -1;
                statsAPIpitches.lastLivePitchIndex = 0;
                if($("#game-select").prop('selectedIndex') !== -1 && statsAPIpitches.liveSelected) {
                    statsAPIpitches.$loading.show();
                    $('#pitcher-select-div')
                        .fadeOut();
                    $('#batter-select-div')
                        .fadeOut();
                    $('#inning-select-div')
                        .fadeOut();
                    $('#live-select').addClass('ui-btn-active ui-state-persist');
                    statsAPIpitches.liveSelected = true;
                    statsAPIpitches.clearLive = true;
                    statsAPIpitches.liveHandler();
                }
                statsAPIpitches.retrieveGameJsons(gameId);
            });
        $('#live-select')
            .mousedown(function(){
                $('#live-select').removeClass("threeDpitch-light-shadowed");
            })
            .mouseup(function(){
                $('#live-select').addClass("threeDpitch-light-shadowed");
            })
            .mouseout(function(){
                $('#live-select').addClass("threeDpitch-light-shadowed");
            })
            .click(function(){
                statsAPIpitches.liveClick();
            });
        await promise;
        statsAPIpitches.$loading.hide();
        gameTitle.text("Please select a game date");
    },

    allOrNoneNullCheck: function(arrToCheck) {
        let anyNull = false;
        let counter = 0;
        let length = arrToCheck.length;
        //console.log("allOrNoneCheck");
        //console.log("length: " + length);
        while(!anyNull && counter < length){
            //console.log("counter: " + counter);
            if(arrToCheck[counter] === null) {
                anyNull = true;
            }
            ++counter;
        }
        return anyNull;
    },

    retrieveGameJsons: async function(gameId) {
        let containerDiv = $('#containerDiv');
        if(containerDiv.length){
            containerDiv.fadeOut("fast");
        }
        let firstPromise = new Promise((resolve) => {
            resolve();
        });
        let storedPitcherPlays = statsAPIpitches.checkStorage(gameId + "_pitcherPlays", true);
        let storedPitchersAvailable = statsAPIpitches.checkStorage(gameId + "_pitchersAvailable", true);
        let storedPitcherNames = statsAPIpitches.checkStorage(gameId + "_pitcherNames", true);
        let storedBatterNames = statsAPIpitches.checkStorage(gameId + "_batterNames", true);
        await firstPromise;
        if(!statsAPIpitches.allOrNoneNullCheck([storedPitcherPlays, storedPitchersAvailable, storedPitcherNames, storedBatterNames])){
            let secondPromise = new Promise((resolve) => {
                resolve();
            });
            //console.log("all stored game data retrieved for gameId: " + gameId);
            statsAPIpitches.pitcherPlaysMap = storedPitcherPlays;
            statsAPIpitches.pitchersAvailable = storedPitchersAvailable;
            statsAPIpitches.pitcherNames = storedPitcherNames;
            statsAPIpitches.batterNames = storedBatterNames;
            await secondPromise;
            statsAPIpitches.populateSelectors();
        } else {
            //console.log("no stored game data retrieved for gameId: " + gameId);
            statsAPIpitches.retrieveGameData(gameId)
        }
    },

    retrieveGameData: function (gameId){
        $.ajax({
            url: "https://statsapi.mlb.com/api/v1.1/game/" + gameId + "/feed/live",
            method: 'GET',
        }).done(async function (data) {
            if(data.hasOwnProperty('liveData') && data.hasOwnProperty('gameData')){
                let liveData = data['liveData'];
                let gameData = data['gameData'];
                if((liveData !== null && liveData !== {}) && (gameData !== null && gameData !== {})){
                    let promise = new Promise((resolve) => {
                        resolve();
                    });
                    if(gameData.hasOwnProperty('players')) {
                        let playersObj = statsAPIpitches.checkStorage(gameId + "_players", true);
                        if(playersObj === null){
                            playersObj = gameData['players'];
                            sessionStorage.setItem(gameId + "_players", JSON.stringify(playersObj));
                            //console.log("No players object found in storage");
                        } else {
                            //console.log("Retrieved players object from storage");
                        }
                        statsAPIpitches.playersJson = playersObj;
                        //console.log(playersObj);
                    }
                    statsAPIpitches.processPlays(liveData['plays']['allPlays'],data['gameData']['datetime']['originalDate'],data['gameData']['teams']['away']['name'],data['gameData']['teams']['home']['name']);
                    await promise;
                    statsAPIpitches.processAverages(gameId);
                } else {
                    statsAPIpitches.noGameDataDialog();
                }
            } else {
                statsAPIpitches.noGameDataDialog();
            }
        }).fail(function (status) {
            statsAPIpitches.noGameDataDialog(status);
        });
    },

    // TODO
    gameNotStartedDialog: function(date, time, ampm){
        let dialogDiv = $('<div id="dialog-message">Game has not started, please try back after ' + time + ampm + ' on ' + date + '</div>');
        dialogDiv.dialog({
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $("#date-select").prop('selectedIndex',-1);
                    $("#game-select").prop('selectedIndex',-1);
                    $('#gameTitle').text("Please select a game date");
                }
            }
        });
        statsAPIpitches.$loading.hide();
    },

    noGameDataDialog: function(status){
        let dialogDiv = $('<div id="dialog-message">Failure retrieving game data: Error '+status+'</div>');
        if(status === null){
            dialogDiv = $('<div id="dialog-message">No game data available</div>');
        }
        dialogDiv.dialog({
            modal: true,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                    $("#date-select").prop('selectedIndex',-1);
                    $("#game-select").prop('selectedIndex',-1);
                    $('#gameTitle').text("Please select a game date");
                }
            }
        });
        statsAPIpitches.$loading.hide();
    },
    
    populateSelectors: async function(){
        let promise = new Promise((resolve) => {
            resolve();
        });
        let pitcherSelect = $("#pitcher-select"), batterSelect = $("#batter-select");
        $.each(statsAPIpitches.pitchersAvailable, function (key) {
            pitcherSelect
                .append('<option value="' + key + '">' + key + ' - ' + statsAPIpitches.pitcherNames[key] +'</option>');
        });
        pitcherSelect.prop('selectedIndex',-1);
        pitcherSelect
            .change(function() {
                if(pitcherSelect.prop('selectedIndex') !== -1 && statsAPIpitches.liveSelected){
                    statsAPIpitches.removeLive();
                }
                if($('#containerDiv').length){
                    $('#containerDiv').fadeOut("fast");
                }
                let pitcherId = $("#pitcher-select option:selected").val();
                //console.log("selected pitcher: " + pitcherId);
                batterSelect
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="average">Average for Game</option>');
                    $.each(statsAPIpitches.pitchersAvailable[pitcherId], function (key) {
                        batterSelect
                            .append('<option value="' + key + '">' + key + ' - ' + statsAPIpitches.batterNames[key] +'</option>');
                    });
                batterSelect.prop('selectedIndex',-1);
                $('#inning-select')
                    .find('option')
                    .remove()
                    .end()
            });
        batterSelect
            .change(function() {
                if(batterSelect.prop('selectedIndex') !== -1 && statsAPIpitches.liveSelected){
                    statsAPIpitches.removeLive();
                }
                statsAPIpitches.lastLiveAtBatIndex = -1;
                statsAPIpitches.lastLivePitchIndex = 0;
                statsAPIpitches.liveSelected = false;
                statsAPIpitches.$loading.show();
                let pitcherId = $('#pitcher-select option:selected').val();
                let batterId = $("#batter-select option:selected").val();
                //console.log("batter selected: " + batterId);
                let averagePitchesMap = statsAPIpitches.pitcherPlaysMap[pitcherId]['averagePitch'];
                if(batterId === "average"){
                    if($('#containerDiv').length){
                        threeDpitches.plotPitches([averagePitchesMap], false, true, false, true, true);
                    } else {
                        threeDpitches.plotPitches([averagePitchesMap], true, true, false, true, true);
                    }
                }
                $('#inning-select')
                    .find('option')
                    .remove()
                    .end();
                if(batterId !== "average") {
                    $.each(statsAPIpitches.pitchersAvailable[pitcherId][batterId], function (key) {
                        $('#inning-select')
                            .append('<option value="' + key + '">' + key + '</option>');
                    });

                    let inning = statsAPIpitches.pitchersAvailable[pitcherId][batterId][Object.keys(statsAPIpitches.pitchersAvailable[pitcherId][batterId])[0]];
                    let playsArray = statsAPIpitches.pitcherPlaysMap[pitcherId][batterId][inning];
                    if ($('#containerDiv').length) {
                        threeDpitches.plotPitches(playsArray, false, false, false, true, true);
                    } else {
                        threeDpitches.plotPitches(playsArray, true, false, false, true, true);
                    }
                }
            });
        $('#inning-select')
            .change(function() {
                if($("#inning-select").prop('selectedIndex') !== -1 && statsAPIpitches.liveSelected){
                    statsAPIpitches.removeLive();
                }
                statsAPIpitches.lastLiveAtBatIndex = -1;
                statsAPIpitches.lastLivePitchIndex = 0;
                statsAPIpitches.liveSelected = false;
                statsAPIpitches.$loading.show();
                let pitcherId = $('#pitcher-select option:selected').val();
                let batterId = $('#batter-select option:selected').val();
                let inning = $("#inning-select option:selected").val();
                let playsArray = statsAPIpitches.pitcherPlaysMap[pitcherId][batterId][inning];
                if($('#containerDiv').length){
                    threeDpitches.plotPitches(playsArray, false, false, false, true, true);
                } else {
                    threeDpitches.plotPitches(playsArray, true, false, false, true, true);
                }

            });
        await promise;
        statsAPIpitches.$loading.hide();
    },

    processAverages: async function(gameId){
        //console.log(statsAPIpitches.pitcherTotals);
        let promise = new Promise((resolve) => {
            resolve();
        });
        $.each(statsAPIpitches.pitcherTotals, function (key1, value1) {
            $.each(value1, function (key2, value2) {
                let count = value2.length;
                let x0 = 0;
                let y0 = 0;
                let z0 = 0;
                let vX0 = 0;
                let vY0 = 0;
                let vZ0 = 0;
                let aX = 0;
                let aY = 0;
                let aZ = 0;
                let breakY = 0;
                let pfxX = 0;
                let pfxZ = 0;
                let startSpeed = 0;
                let endSpeed = 0;
                let strikeZoneTop = 0;
                let strikeZoneBottom = 0;
                let breakAngle = 0;
                let breakLength = 0;
                let spinRate = 0;
                for(let i = 0; i < count; i++){
                    x0 = Number(x0 + value2[i]['x0']);
                    y0 = Number(y0 + value2[i]['y0']);
                    z0 = Number(z0 + value2[i]['z0']);
                    vX0 = Number(vX0 + value2[i]['vX0']);
                    vY0 = Number(vY0 + value2[i]['vY0']);
                    vZ0 = Number(vZ0 + value2[i]['vZ0']);
                    aX = Number(aX + value2[i]['aX']);
                    aY = Number(aY + value2[i]['aY']);
                    aZ = Number(aZ + value2[i]['aZ']);
                    breakY = Number(breakY + value2[i]['breakY']);
                    pfxX = Number(pfxX + value2[i]['pfxX']);
                    pfxZ = Number(pfxZ + value2[i]['pfxZ']);
                    startSpeed = Number(startSpeed + value2[i]['startSpeed']);
                    endSpeed = Number(endSpeed + value2[i]['endSpeed']);
                    strikeZoneTop = Number(strikeZoneTop + value2[i]['strikeZoneTop']);
                    strikeZoneBottom = Number(strikeZoneBottom + value2[i]['strikeZoneBottom']);
                    breakAngle = Number(breakAngle + value2[i]['breakAngle']);
                    breakLength = Number(breakLength + value2[i]['breakLength']);
                    if(!isNaN(value2[i]['spinRate'])){
                        spinRate = Number(spinRate + value2[i]['spinRate']).toFixed(0);
                    }
                }
                x0 = Number(x0/count);
                y0 = Number(y0/count);
                z0 = Number(z0/count);
                vX0 = Number(vX0/count);
                vY0 = Number(vY0/count);
                vZ0 = Number(vZ0/count);
                aX = Number(aX/count);
                aY = Number(aY/count);
                aZ = Number(aZ/count);
                breakY = Number(breakY/count);
                pfxX = Number(pfxX/count);
                pfxZ = Number(pfxZ/count);
                startSpeed = Number(startSpeed/count).toFixed(2);
                endSpeed = Number(endSpeed/count).toFixed(2);
                strikeZoneTop = Number(strikeZoneTop/count);
                strikeZoneBottom = Number(strikeZoneBottom/count);
                breakAngle = Number(breakAngle/count);
                breakLength = Number(breakLength/count);
                spinRate = Number(spinRate/count);
                let averagePitchObj = {};
                let r = Number(vY0/aY);
                let s = Number((2*(y0-statsAPIpitches.plateLength))/aY);
                let time = Number(-r-Math.sqrt((Math.pow(r,2))-s));
                let breakS = Number((2*(y0-breakY))/aY);
                let breakTime = Number(-r-Math.sqrt((Math.pow(r,2))-breakS));
                averagePitchObj.initPath = [x0,y0,z0];
                averagePitchObj.quartPath = [statsAPIpitches.getPosition(x0, time/4, aX, vX0),statsAPIpitches.getPosition(y0, time/4, aY, vY0),statsAPIpitches.getPosition(z0, time/4, aZ, vZ0)];
                averagePitchObj.halfPath = [statsAPIpitches.getPosition(x0, time/2, aX, vX0),statsAPIpitches.getPosition(y0, time/2, aY, vY0),statsAPIpitches.getPosition(z0, time/2, aZ, vZ0)];
                averagePitchObj.threeQuartPath = [statsAPIpitches.getPosition(x0, (time*3)/4, aX, vX0),statsAPIpitches.getPosition(y0, (time*3)/4, aY, vY0),statsAPIpitches.getPosition(z0, (time*3)/4, aZ, vZ0)];
                averagePitchObj.finalPath = [statsAPIpitches.getPosition(x0, time, aX, vX0),0,statsAPIpitches.getPosition(z0, time, aZ, vZ0)];
                averagePitchObj.breakPoint = [statsAPIpitches.getPosition(x0, breakTime, aX, vX0),breakY,statsAPIpitches.getPosition(z0, breakTime, aZ, vZ0)];
                averagePitchObj.pfxX = pfxX;
                averagePitchObj.pfxZ = pfxZ;
                averagePitchObj.startMPH = startSpeed;
                averagePitchObj.endSpeed = endSpeed;
                averagePitchObj.topSZ = strikeZoneTop;
                averagePitchObj.bottomSZ = strikeZoneBottom;
                averagePitchObj.breakAngle = breakAngle;
                averagePitchObj.breakLength = breakLength;
                averagePitchObj.spinRate = Number(spinRate).toFixed(0);
                let newPitch = {};
                newPitch.pitchType = key2;
                newPitch.pitchCoordinates = averagePitchObj;
                //console.log(key1);
                if(statsAPIpitches.pitcherPlaysMap[key1].hasOwnProperty('averagePitch')){
                    let tempAvePitches = statsAPIpitches.pitcherPlaysMap[key1].averagePitch;
                    tempAvePitches[key2] = newPitch;
                    statsAPIpitches.pitcherPlaysMap[key1].averagePitch = tempAvePitches;
                } else {
                    let newAvePitchObj = {};
                    newAvePitchObj[key2] = newPitch;
                    statsAPIpitches.pitcherPlaysMap[key1].averagePitch = newAvePitchObj;
                }
            });
            //console.log("calculated averages");
            //console.log(statsAPIpitches.pitcherPlaysMap[key1].averagePitch);
        });
        await promise;
        //console.log("storing JSON data for gameID: " + gameId);
        sessionStorage.setItem(gameId+ "_pitcherPlays", JSON.stringify(statsAPIpitches.pitcherPlaysMap));
        sessionStorage.setItem(gameId+ "_pitchersAvailable", JSON.stringify(statsAPIpitches.pitchersAvailable));
        sessionStorage.setItem(gameId+ "_pitcherNames", JSON.stringify(statsAPIpitches.pitcherNames));
        sessionStorage.setItem(gameId+ "_batterNames", JSON.stringify(statsAPIpitches.batterNames));
        statsAPIpitches.$loading.hide();
    },

    processPlays: async function(allPlays,gameDate,awayTeam,homeTeam) {
        let promise = new Promise((resolve) => {
            resolve([gameDate,awayTeam,homeTeam]);
        });
        statsAPIpitches.clearTransient();
        for(let i = 0; i < allPlays.length; i++){
            if(allPlays[i].hasOwnProperty('matchup')) {
                let pitcherName = allPlays[i]['matchup']['pitcher']['fullName'];
                let pitcherId = allPlays[i]['matchup']['pitcher']['id'];
                let pitcherHand = allPlays[i]['matchup']['pitchHand']['code'];
                let batterName = allPlays[i]['matchup']['batter']['fullName'];
                let batterId = allPlays[i]['matchup']['batter']['id'];
                let batterSide = allPlays[i]['matchup']['batSide']['code'];
                let inning = allPlays[i]['about']['inning'];
                let halfInning = (allPlays[i]['about']['halfInning']).charAt(0).toUpperCase() + (allPlays[i]['about']['halfInning']).substr(1,2);
                let menOnBase = allPlays[i]['matchup']['splits']['menOnBase'];
                let outs = allPlays[i]['count']['outs'];
                let resultCode = allPlays[i]['result']['event'];
                let resultDesc = allPlays[i]['result']['description'];
                if (!statsAPIpitches.pitcherNames.hasOwnProperty(pitcherId)) {
                    statsAPIpitches.pitcherNames[pitcherId] = pitcherName;
                    //console.log("Pitcher: " + statsAPIpitches.pitcherNames[pitcherId]);
                }
                if (!statsAPIpitches.batterNames.hasOwnProperty(batterId)) {
                    statsAPIpitches.batterNames[batterId] = batterName;
                    //console.log("Batter: " + statsAPIpitches.batterNames[batterId]);
                }
                let batObj = {};
                let pitchObj = {};
                if (statsAPIpitches.pitchersAvailable.hasOwnProperty(pitcherId)) {
                    let tempPitchers = statsAPIpitches.pitchersAvailable[pitcherId];
                    if (tempPitchers.hasOwnProperty(batterId)) {
                        let tempBatters = tempPitchers[batterId];
                        if (!tempBatters.hasOwnProperty(inning)) {
                            tempBatters[inning] = inning;
                            tempPitchers[batterId] = tempBatters;
                            statsAPIpitches.pitchersAvailable[pitcherId] = tempPitchers;
                        }
                    } else {
                        batObj = {};
                        batObj[inning] = inning;
                        tempPitchers[batterId] = batObj;
                        statsAPIpitches.pitchersAvailable[pitcherId] = tempPitchers;
                    }
                } else {
                    batObj = {};
                    batObj[inning] = inning;
                    pitchObj = {};
                    pitchObj[batterId] = batObj;
                    statsAPIpitches.pitchersAvailable[pitcherId] = pitchObj;
                }
                if (statsAPIpitches.pitcherPlaysMap.hasOwnProperty(pitcherId)) {
                    let tempPitcherPlay = statsAPIpitches.pitcherPlaysMap[pitcherId];
                    if (tempPitcherPlay.hasOwnProperty(batterId)) {
                        let tempBatterPlay = tempPitcherPlay[batterId];
                        if (tempBatterPlay.hasOwnProperty(inning)) {
                            let tempInningArr = tempBatterPlay[inning];
                            tempInningArr.push(statsAPIpitches.processPlayEvents(allPlays[i]['playEvents'], pitcherHand, batterSide, menOnBase, outs, resultCode, resultDesc, pitcherId, batterId, inning, halfInning));
                            tempBatterPlay[inning] = tempInningArr;
                            tempPitcherPlay[batterId] = tempBatterPlay;
                            statsAPIpitches.pitcherPlaysMap[pitcherId] = tempPitcherPlay;
                        } else {
                            tempBatterPlay[inning] = [statsAPIpitches.processPlayEvents(allPlays[i]['playEvents'], pitcherHand, batterSide, menOnBase, outs, resultCode, resultDesc, pitcherId, batterId, inning, halfInning)];
                            tempPitcherPlay[batterId] = tempBatterPlay;
                            statsAPIpitches.pitcherPlaysMap[pitcherId] = tempPitcherPlay;
                        }
                    } else {
                        batObj = {};
                        batObj[inning] = [statsAPIpitches.processPlayEvents(allPlays[i]['playEvents'], pitcherHand, batterSide, menOnBase, outs, resultCode, resultDesc, pitcherId, batterId, inning, halfInning)];
                        tempPitcherPlay[batterId] = batObj;
                        statsAPIpitches.pitcherPlaysMap[pitcherId] = tempPitcherPlay;
                    }
                } else {
                    batObj = {};
                    batObj[inning] = [statsAPIpitches.processPlayEvents(allPlays[i]['playEvents'], pitcherHand, batterSide, menOnBase, outs, resultCode, resultDesc, pitcherId, batterId, inning, halfInning)];
                    pitchObj = {};
                    pitchObj[batterId] = batObj;
                    statsAPIpitches.pitcherPlaysMap[pitcherId] = pitchObj;
                }
            }
        }
        let arr = await promise;
        statsAPIpitches.populateSelectors(arr);
    },

    processPlayEvents: function(playEvents, pitcherHand, batterSide, menOnBase, outs, resultCode, resultDesc, pitcherId, batterId, inning, halfInning){
        let eventsMap = {};
        let batterKey = 'ID' + batterId;
        //console.log("batterKey: " + batterKey);
        eventsMap.szTop = statsAPIpitches.playersJson["" + batterKey+ ""].strikeZoneTop;
        eventsMap.szBot = statsAPIpitches.playersJson["" + batterKey+ ""].strikeZoneBottom;
        eventsMap.inning = inning;
        eventsMap.halfInning = halfInning;
        //console.log("HalfInning: " + halfInning);
        let length = playEvents.length;
        for(let i = 0; i < length; i++){
            let eventMap = {};
            if(playEvents[i]['isPitch'] === true){
                if( playEvents[i]['details'].hasOwnProperty('type')){
                    if( playEvents[i]['details']['type'].hasOwnProperty('code')){
                        eventMap.pitchType = playEvents[i]['details']['type']['code'];
                    } else {
                        //console.log("Unknown Event Pitch Data:");
                        //console.log(playEvents[i]['pitchData']);
                        eventMap.pitchType = 'UNK';
                    }
                } else {
                    //console.log("Unknown Event Pitch Data:");
                    //console.log(playEvents[i]['pitchData']);
                    eventMap.pitchType = 'UNK';
                }
                eventMap.balls = playEvents[i]['count']['balls'];
                eventMap.resultCode = resultCode;
                eventMap.resultDesc = resultDesc;
                eventMap.strikes = playEvents[i]['count']['strikes'];
                eventMap.outcomeCode = playEvents[i]['details']['code'];
                eventMap.outcomeDesc = playEvents[i]['details']['description'];
                eventMap.pitchHand = pitcherHand;
                eventMap.batterSide = batterSide;
                eventMap.menOnBase = menOnBase;
                eventMap.outs = outs;
                eventMap.pitchNumber = playEvents[i]['pitchNumber'];
                eventMap.pitchCoordinates = statsAPIpitches.calculateCoordinates(playEvents[i]['pitchData'], pitcherId, eventMap.pitchType);
                let pitchNumber = playEvents[i]['pitchNumber'];
                eventsMap[pitchNumber] = eventMap;
            }
        }
        return eventsMap;
    },

    calculateCoordinates: function (pitchData, pitcherId, pitchType){
        let pitchCoordinates = {};
        let x0 = Number(pitchData['coordinates']['x0']);
        let y0 = Number(pitchData['coordinates']['y0']);
        let z0 = Number(pitchData['coordinates']['z0']);
        let vX0 = Number(pitchData['coordinates']['vX0']);
        let vY0 = Number(pitchData['coordinates']['vY0']);
        let vZ0 = Number(pitchData['coordinates']['vZ0']);
        let aX = Number(pitchData['coordinates']['aX']);
        let aY = Number(pitchData['coordinates']['aY']);
        let aZ = Number(pitchData['coordinates']['aZ']);
        let breakY = Number(pitchData['breaks']['breakY']);
        let spinRate = 0;
        if(!isNaN(pitchData['breaks']['spinRate'])){
            spinRate = Number(pitchData['breaks']['spinRate']).toFixed(0);
        }
        let r = Number(vY0/aY);
        let s = Number((2*(y0-statsAPIpitches.plateLength))/aY);
        let time = Number(-r-Math.sqrt((Math.pow(r,2))-s));
        let breakS = Number((2*(y0-breakY))/aY);
        let breakTime = Number(-r-Math.sqrt((Math.pow(r,2))-breakS));
        pitchCoordinates.breakTime = breakTime;
        //console.log("pitchData['strikeZoneTop']: " + pitchData['strikeZoneTop']);
        pitchCoordinates.topSZ = pitchData['strikeZoneTop'];
        pitchCoordinates.bottomSZ = pitchData['strikeZoneBottom'];
        //console.log("pitchData['strikeZoneBottom']: " + pitchData['strikeZoneBottom']);
        pitchCoordinates.startMPH = pitchData['startSpeed'];
        pitchCoordinates.endMPH = pitchData['endSpeed'];
        pitchCoordinates.pfxx = pitchData['coordinates']['pfxX'];
        pitchCoordinates.pfxz = pitchData['coordinates']['pfxZ'];
        pitchCoordinates.breakAngle = pitchData['breaks']['breakAngle'];
        pitchCoordinates.breakLength = pitchData['breaks']['breakLength'];
        pitchCoordinates.breakY = breakY;
        pitchCoordinates.spinRate = spinRate;
        pitchCoordinates.initPath = [x0,y0,z0];
        pitchCoordinates.quartPath = [statsAPIpitches.getPosition(x0, time/4, aX, vX0),statsAPIpitches.getPosition(y0, time/4, aY, vY0),statsAPIpitches.getPosition(z0, time/4, aZ, vZ0)];
        pitchCoordinates.halfPath = [statsAPIpitches.getPosition(x0, time/2, aX, vX0),statsAPIpitches.getPosition(y0, time/2, aY, vY0),statsAPIpitches.getPosition(z0, time/2, aZ, vZ0)];
        pitchCoordinates.threeQuartPath = [statsAPIpitches.getPosition(x0, (time*3)/4, aX, vX0),statsAPIpitches.getPosition(y0, (time*3)/4, aY, vY0),statsAPIpitches.getPosition(z0, (time*3)/4, aZ, vZ0)];
        pitchCoordinates.finalPath = [statsAPIpitches.getPosition(x0, time, aX, vX0),0,statsAPIpitches.getPosition(z0, time, aZ, vZ0)];
        pitchCoordinates.breakPoint = [statsAPIpitches.getPosition(x0, breakTime, aX, vX0),breakY,statsAPIpitches.getPosition(z0, breakTime, aZ, vZ0)];
        if(pitchType !== 'UNK') {
            let newCoordArr = [];
            let coordObj = {};
            if (statsAPIpitches.pitcherTotals.hasOwnProperty(pitcherId)) {
                let tempPitchObj = statsAPIpitches.pitcherTotals[pitcherId];
                if (tempPitchObj.hasOwnProperty(pitchType)) {
                    let tempCoordArr = tempPitchObj[pitchType];
                    coordObj = {
                        'x0': x0,
                        'y0': y0,
                        'z0': z0,
                        'vX0': vX0,
                        'vY0': vY0,
                        'vZ0': vZ0,
                        'aX': aX,
                        'aY': aY,
                        'aZ': aZ,
                        'breakY': breakY,
                        'spinRate': spinRate,
                        'pfxX': pitchData['pfxX'],
                        'pfxZ': pitchData['pfxZ'],
                        'startSpeed': pitchData['startSpeed'],
                        'endSpeed': pitchData['endSpeed'],
                        'strikeZoneTop': pitchData['strikeZoneTop'],
                        'strikeZoneBottom': pitchData['strikeZoneBottom'],
                        'breakAngle': pitchData['breaks']['breakAngle'],
                        'breakLength': pitchData['breaks']['breakLength']
                    };
                    tempCoordArr.push(coordObj);
                    tempPitchObj[pitchType] = tempCoordArr;
                    statsAPIpitches.pitcherTotals[pitcherId] = tempPitchObj;
                } else {
                    newCoordArr = [];
                    coordObj = {
                        'x0': x0,
                        'y0': y0,
                        'z0': z0,
                        'vX0': vX0,
                        'vY0': vY0,
                        'vZ0': vZ0,
                        'aX': aX,
                        'aY': aY,
                        'aZ': aZ,
                        'breakY': breakY,
                        'spinRate': spinRate,
                        'pfxX': pitchData['pfxX'],
                        'pfxZ': pitchData['pfxZ'],
                        'startSpeed': pitchData['startSpeed'],
                        'endSpeed': pitchData['endSpeed'],
                        'strikeZoneTop': pitchData['strikeZoneTop'],
                        'strikeZoneBottom': pitchData['strikeZoneBottom'],
                        'breakAngle': pitchData['breaks']['breakAngle'],
                        'breakLength': pitchData['breaks']['breakLength']
                    };
                    newCoordArr.push(coordObj);
                    tempPitchObj[pitchType] = newCoordArr;
                    statsAPIpitches.pitcherTotals[pitcherId] = tempPitchObj;
                }
            } else {
                let newPitchObj = {};
                newCoordArr = [];
                coordObj = {
                    'x0': x0,
                    'y0': y0,
                    'z0': z0,
                    'vX0': vX0,
                    'vY0': vY0,
                    'vZ0': vZ0,
                    'aX': aX,
                    'aY': aY,
                    'aZ': aZ,
                    'breakY': breakY,
                    'spinRate': spinRate,
                    'pfxX': pitchData['pfxX'],
                    'pfxZ': pitchData['pfxZ'],
                    'startSpeed': pitchData['startSpeed'],
                    'endSpeed': pitchData['endSpeed'],
                    'strikeZoneTop': pitchData['strikeZoneTop'],
                    'strikeZoneBottom': pitchData['strikeZoneBottom'],
                    'breakAngle': pitchData['breaks']['breakAngle'],
                    'breakLength': pitchData['breaks']['breakLength']
                };
                newCoordArr.push(coordObj);
                newPitchObj[pitchType] = newCoordArr;
                statsAPIpitches.pitcherTotals[pitcherId] = newPitchObj;
            }
        }
        return pitchCoordinates;
    },

    getPosition: function(initPos, time, accel, velo){
        let accelCalc = Number(0.5*accel*(Math.pow(time,2)));
        let veloCalc = Number(velo*time);
        return Number(accelCalc+veloCalc+initPos);
    },
};