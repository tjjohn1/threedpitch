let threeDpitches = {

    animationFrame: {},
    scene: {},
    renderer:{},
    camera: {}, 
    controls: {}, 
    pitchesGroup: new THREE.Group(),
    gridGroup: new THREE.Group(),
    strikeZoneGroup: new THREE.Group(),
    strikeZoneTempGroup: new THREE.Group(),
    allSZGroup: new THREE.Group(),
    fieldGroup: {},
    gridStorage: {},
    container: {},
    guidMap1: {},
    guidMap2: {},
    guidMap3: {},
    guidMap4: {},
    genSZTop: 3.5,
    genSZBot: 1.5,
    szSide: 2.5,
    szSideLength: 2.4,
    szTempSide: 0,
    szTempBot: 0,
    szTempTop: 0,
    szSideLengthTemp: 0,
    curView: "",
    raycaster: new THREE.Raycaster(),
    mouse: {},
    playsArray: [],
    tempSZAdded: false,
    yPos: 0,
    clipPlanePitches: [new THREE.Plane(new THREE.Vector3(0, 0, 1), 0)],
    averagePlot: false,
    clippingHelpers: new THREE.Group(),
    curPitchTypes: {},
    liveLastStrikeCount: 0,
    liveLastPitchCount: 0,
    initialClean: true,
    animating: false,
    rect:{},
    objects:{},
    intersects:[],
    timeout:undefined,
    moving:false,
    clipPlanes: [
        new THREE.Plane(new THREE.Vector3(0, 0, 1), 0),
        new THREE.Plane(new THREE.Vector3(1, 1, 0), 0),
        new THREE.Plane(new THREE.Vector3(-1, 1, 0), 0)
    ],
    arrowKeysMap: {
        '37':'left_arrow',
        '38':'up_arrow',
        '39':'right_arrow',
        '40':'down_arrow'
    },

    guid: function() {
        return threeDpitches.s4() + threeDpitches.s4() + '-' + threeDpitches.s4() + '-' + threeDpitches.s4() + '-' +
            threeDpitches.s4() + '-' + threeDpitches.s4() + threeDpitches.s4() + threeDpitches.s4();
    },

    s4: function() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    },

    pitchTypeColor: function(pitchType) {
        switch (pitchType) {
            case 'FF':
                return '#ff0000';
            case 'FT':
                return '#ff0000';
            case 'SL':
                return '#ff7619';
            case 'FS':
                return '#0096ff';
            case 'SI':
                return '#ffbe00';
            case 'CH':
                return '#0096ff';
            case 'FC':
                return '#008000';
            case 'CU':
                return '#00008b';
            case 'KC':
                return '#9400d3';
            case 'UNK':
                return '#d3d3d3';
            case 'XX':
                return '#d3d3d3';
        }
    },

    pitchSpeedColor: function(pitchSpeed) {
        if (pitchSpeed < 70) {
            return '#6c0000';
        } else if (pitchSpeed < 75) {
            return '#850000';
        } else if (pitchSpeed < 80) {
            return '#9e0000';
        } else if (pitchSpeed < 85) {
            return '#b70000'
        } else if (pitchSpeed < 90) {
            return '#d00000';
        } else if (pitchSpeed < 95) {
            return '#e90000'
        } else if (pitchSpeed < 100) {
            return '#ff0303';
        } else {
            return '#ff0303';
        }
    },

    pitchRpmColor: function(pitchRpm) {
        if (pitchRpm < 1500) {
            return '#002182';
        } else if (pitchRpm < 1750) {
            return '#00279b';
        } else if (pitchRpm < 2000) {
            return '#002eb4';
        } else if (pitchRpm < 2250) {
            return '#0034cd'
        } else if (pitchRpm < 2500) {
            return '#003ae6'
        } else if (pitchRpm < 2750) {
            return '#0040ff'
        } else if (pitchRpm < 3000) {
            return '#1a53ff';
        } else {
            return '#3466ff';
        }
    },

    pitchOutcomeColor: function(pitchOutcome) {
        switch (pitchOutcome) {
            case 'Single':
                return 'yellow';
            case 'Double':
                return 'purple';
            case 'Triple':
                return 'magenta';
            case 'Home Run':
                return 'red';
            case 'Out':
                return 'green';
            case 'Error':
                return 'green';
            case 'Whiff':
                return 'gray';
            case 'Ball':
                return 'gray';
            case 'Strike':
                return 'gray';
            case 'Foul':
                return 'gray';
        }
    },

    inningString: function(inning) {
        switch (inning) {
            case 1:
                return '1st';
            case 2:
                return '2nd';
            case 3:
                return '3rd';
            case 4:
                return '4th';
            case 5:
                return '5th';
            case 6:
                return '6th';
            case 7:
                return '7th';
            case 8:
                return '8th';
            case 9:
                return '9th';
            case 10:
                return '10th';
            case 11:
                return '11th';
            case 12:
                return '12th';
            case 13:
                return '13th';
            case 14:
                return '14th';
            case 15:
                return '15th';
            case 16:
                return '16th';
            case 17:
                return '17th';
            case 18:
                return '18th';
            case 19:
                return '19th';
            case 20:
                return '20th';
            case 21:
                return '21st';
        }
    },

    pitchTypes: {
        'FF': {
            'shortDesc': 'FF',
            'description': 'Four-seam fastball',
            'color': '#ff0000'
        },
        'FT': {
            'shortDesc': 'FT',
            'description': 'Two-seam fastball',
            'color': '#ff0000'
        },
        'SL': {
            'shortDesc': 'SL',
            'description': 'Slider',
            'color': '#ff7619'
        },
        'SI': {
            'shortDesc': 'SI',
            'description': 'Sinker',
            'color': '#ffbe00'
        },
        'FS': {
            'shortDesc': 'FS',
            'description': 'Splitter',
            'color': '#0096ff'
        },
        'CH': {
            'shortDesc': 'CH',
            'description': 'Changeup',
            'color': '#0096ff'
        },
        'FC': {
            'shortDesc': 'FC',
            'description': 'Cutter',
            'color': '#008000'
        },
        'CU': {
            'shortDesc': 'CU',
            'description': 'Curveball',
            'color': '#00008b'
        },
        'KC': {
            'shortDesc': 'KC',
            'description': 'Knuckle Curve',
            'color': '#9400d3'
        },
        'KN': {
            'shortDesc': 'KN',
            'description': 'Knuckle',
            'color': '#9400d3'
        },
        'UNK': {
            'shortDesc': 'UNKNOWN',
            'description': 'Unknown',
            'color': '#d3d3d3'
        },
        'XX': {
            'shortDesc': 'UNKNOWN',
            'description': 'Unknown',
            'color': '#d3d3d3'
        }
    },

    pitchSpeeds: {
        '70': {
            'shortDesc': '70',
            'description': 'under 70 MPH',
            'color': '#6c0000'
        },
        '75': {
            'shortDesc': '75',
            'description': '70 - 75 MPH',
            'color': '#850000'
        },
        '80': {
            'shortDesc': '80',
            'description': '75 - 80 MPH',
            'color': '#9e0000'
        },
        '85': {
            'shortDesc': '85',
            'description': '80 - 85 MPH',
            'color': '#b70000'
        },
        '90': {
            'shortDesc': '90',
            'description': '85 - 89 MPH',
            'color': '#d00000'
        },
        '95': {
            'shortDesc': '95',
            'description': '90 - 95 MPH',
            'color': '#e90000'
        },
        '100': {
            'shortDesc': '100',
            'description': '95 - 99 MPH',
            'color': '#ff0303'
        },
        '101': {
            'shortDesc': '101',
            'description': '100+ MPH',
            'color': '#ff1d1d'
        }
    },
    
    pitchRpms: {
        '1500': {
            'shortDesc': '1500',
            'description': 'under 1500 RPM',
            'color': '#002182'
        },
        '1750': {
            'shortDesc': '1750',
            'description': '1500 - 1750 RPM',
            'color': '#00279b'
        },
        '2000': {
            'shortDesc': '2000',
            'description': '1750 - 2000 RPM',
            'color': '#002eb4'
        },
        '2250': {
            'shortDesc': '2250',
            'description': '2000 - 2250 RPM',
            'color': '#0034cd'
        },
        '2500': {
            'shortDesc': '2500',
            'description': '2250 - 2500 RPM',
            'color': '#003ae6'
        },
        '2750': {
            'shortDesc': '2750',
            'description': '2500 - 2750 RPM',
            'color': '#0040ff'
        },
        '3000': {
            'shortDesc': '3000',
            'description': '2750 - 3000 RPM',
            'color': '#1a53ff'
        },
        '3001': {
            'shortDesc': '3001',
            'description': '3000+ RPM',
            'color': '#3466ff'
        }
    },

    finalOutcomeCodes: {
      'Sac Fly':'Out',
      'Single': 'Single',
      'Groundout': 'Out',
      'Sac Bunt': 'Out',
      'Strikeout': 'Out',
      'Lineout': 'Out',
      'Flyout': 'Out',
      'Walk': 'Ball',
      'Home Run': 'Home Run',
      'Forceout': 'Out',
      'Double': 'Double',
      'Triple': 'Triple',
      'Pop Out': 'Out',
      'Grounded Into TP': 'Triple',
      'Grounded Into DP': 'Double',
      'Grounded Into SP': 'Single',
      'Field Error':'Error'
    },

    calculateCounts: function(balls, strikes, outs, pitch, totalPitches, outcomeCode, finalOutcome, lastStrikeCount) {
        if (threeDpitches.finalOutcomeCodes[finalOutcome] === 'Out'){
            outs--;
        } else if(threeDpitches.outcomeCodes[outcomeCode] === 'Out') {
            outs--;
        }
        if(pitch === 1){
            strikes = 0;
            balls = 0;
        } else {
            if(balls === 4) {
                if (finalOutcome === 'Walk' ) {
                    balls--;
                } else if (threeDpitches.outcomeCodes[outcomeCode] === 'Ball') {
                    balls--;
                }
            } else if(balls >= 1 ) {
                if (threeDpitches.finalOutcomeCodes[finalOutcome] === 'Ball') {
                    balls--;
                } else if (threeDpitches.outcomeCodes[outcomeCode] === 'Ball') {
                    balls--;
                }
            }
            if(strikes === 3) {
                if (threeDpitches.finalOutcomeCodes[finalOutcome] === 'Out') {
                    lastStrikeCount = strikes;
                    strikes--;
                } else if (threeDpitches.outcomeCodes[outcomeCode] === 'Strike') {
                    lastStrikeCount = strikes;
                    strikes--;
                }
            } else if(strikes >= 1 ) {
                if (threeDpitches.finalOutcomeCodes[finalOutcome] === 'Strike') {
                    lastStrikeCount = strikes;
                    strikes--;
                } else if (threeDpitches.outcomeCodes[outcomeCode] === 'Strike') {
                    lastStrikeCount = strikes;
                    strikes--;
                } else if (threeDpitches.outcomeCodes[outcomeCode] === 'Foul'){
                    if(lastStrikeCount < 2){
                        lastStrikeCount = strikes;
                        strikes--;
                    }
                }
            }
        }
        return [balls, strikes, outs, lastStrikeCount];
    },

    outcomeCodes: {
      'C': 'Strike',
      'S': 'Strike',
      'X': 'Out',
      'B': 'Ball',
      'F': 'Foul',
      'E': 'Run',
      'D': 'InPlay',
      'W': 'Strike',
      'T': 'Foul',
      '*B':'*Ball'
    },
    
    pitchOutcomes: {
        'Single': {
            'shortDesc': 'SGL',
            'description': 'Single',
            'color': 'yellow',
            'group': 'Single'
        },
        'Double': {
            'shortDesc': 'DBL',
            'description': 'Double',
            'color': 'purple',
            'group': 'Double'
        },
        'Triple': {
            'shortDesc': 'TPL',
            'description': 'Triple',
            'color': 'magenta',
            'group': 'Triple'
        },
        'Home Run': {
            'shortDesc': 'HR',
            'description': 'Home Run',
            'color': 'red',
            'group': 'Home Run'
        },
        'Out': {
            'shortDesc': 'OUT',
            'description': 'Out',
            'color': 'green',
            'group': 'Out or Error'
        },
        'Error': {
            'shortDesc': 'ERR',
            'description': 'Error',
            'color': 'green',
            'group': 'Out or Error'
        },
        'Whiff': {
            'shortDesc': 'WHF',
            'description': 'Whiff',
            'color': 'gray',
            'group': 'Whiff or Called Pitch'
        },
        'Ball': {
            'shortDesc': 'BL',
            'description': 'Ball',
            'color': 'gray',
            'group': 'Whiff or Called Pitch'
        },
        'Strike': {
            'shortDesc': 'STR',
            'description': 'Strike',
            'color': 'gray',
            'group': 'Whiff or Called Pitch'
        },
        'Foul': {
            'shortDesc': 'FL',
            'description': 'Foul',
            'color': 'gray',
            'group': 'Whiff or Called Pitch'
        },
        '*Ball': {
            'shortDesc': 'BL',
            'description': 'Ball in Dirt',
            'color': 'gray',
            'group': 'Whiff or Called Pitch'
        },
    },
    
    pitchOutcomeGroups: {
        'Single': {
            'shortDesc': 'SGL',
            'description': 'Single',
            'color': 'yellow',
            'group': 'Single'
        },
        'Double': {
            'shortDesc': 'DBL',
            'description': 'Double',
            'color': 'purple',
            'group': 'Double'
        },
        'Triple': {
            'shortDesc': 'TPL',
            'description': 'Triple',
            'color': 'magenta',
            'group': 'Triple'
        },
        'Home Run': {
            'shortDesc': 'HR',
            'description': 'Home Run',
            'color': 'red',
            'group': 'Home Run'
        },
        'Out or Error': {
            'shortDesc': 'OOE',
            'description': 'Out or Error',
            'color': 'green',
            'group': 'Out or Error'
        },
        'Whiff or Called Pitch': {
            'shortDesc': 'WOC',
            'description': 'Whiff or Called Pitch',
            'color': 'gray',
            'group': 'Whiff or Called Pitch'
        }
    },

    addTempStrikeZone: function() {
        if(!threeDpitches.tempSZAdded) {
            let strikeZoneObjects = threeDpitches.strikeZoneGroup.children;
            for (let k = 0; k < strikeZoneObjects.length; k++) {
                strikeZoneObjects[k].material.visible = false;
            }
            let strikeZoneTempPoints = [
                new THREE.Vector3(-0.7083, 0, threeDpitches.szTempSide),
                new THREE.Vector3(0, 0, threeDpitches.szTempBot),
                new THREE.Vector3(0.7083, 0, threeDpitches.szTempSide),
                new THREE.Vector3(0, 0, threeDpitches.szTempTop)
            ];

            // material
            let strikeZoneTempMaterial = new THREE.MeshLambertMaterial({
                color: "blue",
                side: THREE.DoubleSide,
                transparent: true,
                opacity: 0.5
            });

            for (let j = 0; j < strikeZoneTempPoints.length; j++) {
                let lengthTemp = 0;
                let rotationXTemp = 0;
                let rotationYTemp = 0;
                let rotationZTemp = 0;
                if (j === 0 || j === 2) {
                    lengthTemp = threeDpitches.szSideLengthTemp;
                    rotationXTemp = Math.PI / 2;
                } else {
                    lengthTemp = 1.8;
                    rotationZTemp = Math.PI / 2;
                }
                let strikeZoneTempGeometry = new THREE.CylinderGeometry(0.025, 0.025, lengthTemp, 32);

                strikeZoneTempGeometry.mergeVertices();
                strikeZoneTempGeometry.computeVertexNormals(true);
                strikeZoneTempGeometry.computeFaceNormals();

                let strikeZoneTempHelper = new THREE.Mesh(strikeZoneTempGeometry, strikeZoneTempMaterial);

                threeDpitches.strikeZoneTempGroup.add(strikeZoneTempHelper);
                switch (j) {
                    case 0:
                        strikeZoneTempHelper.position.copy(strikeZoneTempPoints[0]);
                        strikeZoneTempHelper.rotation.x = rotationXTemp;
                        strikeZoneTempHelper.rotation.y = rotationYTemp;
                        strikeZoneTempHelper.rotation.z = rotationZTemp;
                        threeDpitches.szTempLeftMesh = strikeZoneTempHelper;
                        strikeZoneTempHelper.part = 'left';
                        break;
                    case 1:
                        strikeZoneTempHelper.position.copy(strikeZoneTempPoints[1]);
                        strikeZoneTempHelper.rotation.x = rotationXTemp;
                        strikeZoneTempHelper.rotation.y = rotationYTemp;
                        strikeZoneTempHelper.rotation.z = rotationZTemp;
                        threeDpitches.szTempBottomMesh = strikeZoneTempHelper;
                        strikeZoneTempHelper.part = 'bottom';
                        break;
                    case 2:
                        strikeZoneTempHelper.position.copy(strikeZoneTempPoints[2]);
                        strikeZoneTempHelper.rotation.x = rotationXTemp;
                        strikeZoneTempHelper.rotation.y = rotationYTemp;
                        strikeZoneTempHelper.rotation.z = rotationZTemp;
                        threeDpitches.szTempRightMesh = strikeZoneTempHelper;
                        strikeZoneTempHelper.part = 'right';
                        break;
                    case 3:
                        strikeZoneTempHelper.position.copy(strikeZoneTempPoints[3]);
                        strikeZoneTempHelper.rotation.x = rotationXTemp;
                        strikeZoneTempHelper.rotation.y = rotationYTemp;
                        strikeZoneTempHelper.rotation.z = rotationZTemp;
                        threeDpitches.szTempTopMesh = strikeZoneTempHelper;
                        strikeZoneTempHelper.part = 'top';
                        break;
                    default:
                        break;
                }
            }
            threeDpitches.allSZGroup.add(threeDpitches.strikeZoneTempGroup);
            threeDpitches.tempSZAdded = true;
            threeDpitches.postRender();
        }
    },

    addStrikeZone: function() {
        let szGroupLength = threeDpitches.strikeZoneGroup.children.length;
        if(szGroupLength > 0) {
            threeDpitches.disposeStrikeZone();
        }

        let strikeZonePoints = [
            new THREE.Vector3(-0.7083, 0, threeDpitches.szSide),
            new THREE.Vector3(0, 0, threeDpitches.genSZBot),
            new THREE.Vector3(0.7083, 0, threeDpitches.szSide),
            new THREE.Vector3(0, 0, threeDpitches.genSZTop)
        ];

        // material
        let strikeZoneMaterial = new THREE.MeshLambertMaterial({
            color: "aqua",
            side: THREE.DoubleSide,
            transparent: true,
            opacity: 0.5
        });

        for (let i = 0; i < strikeZonePoints.length; i++) {
            let length = 0;
            let rotationX = 0;
            let rotationY = 0;
            let rotationZ = 0;
            if (i === 0 || i === 2) {
                length = strikeZonePoints[3].z - strikeZonePoints[1].z + .4;
                rotationX = Math.PI / 2;
            } else {
                length = 1.8;
                rotationZ = Math.PI / 2;
            }
            let strikeZoneGeometry = new THREE.CylinderGeometry(0.025, 0.025, length, 64);

            strikeZoneGeometry.mergeVertices();
            strikeZoneGeometry.computeVertexNormals(true);
            strikeZoneGeometry.computeFaceNormals();

            const strikeZoneHelper = new THREE.Mesh(strikeZoneGeometry, strikeZoneMaterial);

            strikeZoneHelper.position.copy(strikeZonePoints[i]);
            strikeZoneHelper.rotation.x = rotationX;
            strikeZoneHelper.rotation.y = rotationY;
            strikeZoneHelper.rotation.z = rotationZ;
            threeDpitches.strikeZoneGroup.add(strikeZoneHelper);
            switch (i) {
                case 0:
                    threeDpitches.szLeft = strikeZoneHelper;
                    strikeZoneHelper.part = 'left';
                    break;
                case 1:
                    threeDpitches.szBottom = strikeZoneHelper;
                    strikeZoneHelper.part = 'bottom';
                    break;
                case 2:
                    threeDpitches.szRight = strikeZoneHelper;
                    strikeZoneHelper.part = 'right';
                    break;
                case 3:
                    threeDpitches.szTop = strikeZoneHelper;
                    strikeZoneHelper.part = 'top';
                    break;
                default:
                    break;
            }
        }
        threeDpitches.allSZGroup.add(threeDpitches.strikeZoneGroup);
        if($('#container').length){
            threeDpitches.postRender();
        } else {
            threeDpitches.scene.add(threeDpitches.allSZGroup);
        }
    },

    isObject: function(value) {
        return value && typeof value === 'object' && value.constructor === Object;
    },

    processPitches: async function(playsArray, isInitial, isAverage, isLive, isComplete, clearLive) {
        if(isComplete){
            threeDpitches.liveLastPitchCount = 0;
        }
        let lastStrikeCount = 0;
        threeDpitches.curPitchTypes = {};
        let promiseGuidRemove = new Promise((resolve) => {
            resolve();
        });
        if(!isInitial){
            if(clearLive){
                threeDpitches.disposePitches();
                threeDpitches.guidMap1 = {};
                threeDpitches.guidMap2 = {};
                threeDpitches.guidMap3 = {};
                threeDpitches.guidMap4 = {};
            } else {
                lastStrikeCount = threeDpitches.liveLastStrikeCount;
            }
        }

        await promiseGuidRemove;
        let promise = new Promise((resolve) => {
            resolve();
        });
        let countsArr = [], points = [];
        let topSZTot = 0, botSZTot = 0, counter = 0;
        let cylinderGeometry = new THREE.CylinderGeometry(0.1, 0.1, 0.1, 32);
        let ballGeometry = new THREE.SphereGeometry(0.05, 64, 64);
        let pitchTypeDiv, pitchSpeedDiv, pitchRpmDiv, pitchCountDiv, ballsStrikesOutsDiv, inningDiv, matchupDiv, pitchOutcomeDiv;
        let coordObj, sidePos, curGuid, sideLength;

        let arrLength = playsArray.length;
        for(let i = 0; i< arrLength; i++) {
            let eventsMap = playsArray[i];
            threeDpitches.genSZTop = eventsMap.szTop;
            threeDpitches.genSZBot = eventsMap.szBot;
            let inning = eventsMap.inning;
            let halfInning = eventsMap.halfInning;
            threeDpitches.szSide = Number((threeDpitches.genSZTop - threeDpitches.genSZBot)/2 + threeDpitches.genSZBot);
            threeDpitches.szSideLength = Number((threeDpitches.genSZTop - threeDpitches.genSZBot) + 0.4);
            let mapLength = (Object.keys(eventsMap)).length - 4;
            if(isAverage){
                mapLength = (Object.keys(eventsMap)).length;
            }
            let totalPitches = mapLength;
            if(!clearLive) {
                totalPitches = Number(threeDpitches.liveLastPitchCount + mapLength);
            }
            $.each(eventsMap, function (key, value) {
                if(threeDpitches.isObject(value)) {
                    if(isAverage){
                        topSZTot = Number(topSZTot + value.pitchCoordinates.topSZ);
                        botSZTot = Number(botSZTot + value.pitchCoordinates.bottomSZ);
                        ++counter;
                        if(counter === mapLength){
                            threeDpitches.genSZTop = Number(topSZTot/mapLength);
                            threeDpitches.genSZBot = Number(botSZTot/mapLength);
                            threeDpitches.szSide = Number((threeDpitches.genSZTop - threeDpitches.genSZBot)/2 + threeDpitches.genSZBot);
                            threeDpitches.szSideLength = Number((threeDpitches.genSZTop - threeDpitches.genSZBot) + 0.4);
                        }
                    }

                    countsArr = threeDpitches.calculateCounts(value['balls'], value['strikes'], value['outs'], key, mapLength, value['outcomeCode'], value['resultCode'], lastStrikeCount);
                    lastStrikeCount = countsArr[3];
                    if (isLive && key == mapLength) {
                        threeDpitches.liveLastStrikeCount = lastStrikeCount;
                        if (!clearLive) {
                            threeDpitches.liveLastPitchCount = Number(threeDpitches.liveLastPitchCount + key);
                        } else {
                            threeDpitches.liveLastPitchCount = Number(key);
                        }
                    }

                    coordObj = value.pitchCoordinates;
                    sidePos = (value.pitchCoordinates.topSZ - value.pitchCoordinates.bottomSZ) / 2 + value.pitchCoordinates.bottomSZ;
                    curGuid = threeDpitches.guid();
                    sideLength = (coordObj.topSZ - coordObj.bottomSZ) + .4;

                    // points
                    points = [];

                    points.push(new THREE.Vector3(coordObj['initPath'][0], coordObj['initPath'][1], coordObj['initPath'][2]));
                    points.push(new THREE.Vector3(coordObj['quartPath'][0], coordObj['quartPath'][1], coordObj['quartPath'][2]));
                    points.push(new THREE.Vector3(coordObj['halfPath'][0], coordObj['halfPath'][1], coordObj['halfPath'][2]));
                    points.push(new THREE.Vector3(coordObj['threeQuartPath'][0], coordObj['threeQuartPath'][1], coordObj['threeQuartPath'][2]));
                    points.push(new THREE.Vector3(coordObj['finalPath'][0], coordObj['finalPath'][1], coordObj['finalPath'][2]));

                    let color = threeDpitches.pitchTypeColor(value.pitchType);

                    // path
                    let path = new THREE.CatmullRomCurve3(points);

                    // params
                    let pathSegments = 128;
                    let tubeRadius = 0.05;
                    let radiusSegments = 32;
                    let closed = false;

                    // geometry
                    let tubeGeometry = new THREE.TubeGeometry(path, pathSegments, tubeRadius, radiusSegments, closed);

                    tubeGeometry.mergeVertices();
                    tubeGeometry.computeVertexNormals(true);
                    tubeGeometry.computeFaceNormals();
                    // to buffer geometry
                    let bufferGeometry = new THREE.BufferGeometry().fromGeometry(tubeGeometry);
                    threeDpitches.nMax = bufferGeometry.attributes.position.count;

                    // material
                    let material = new THREE.MeshLambertMaterial({
                        color: color,
                        side: THREE.DoubleSide,
                        opacity: 1.0,
                        transparent: true,
                        clippingPlanes: [threeDpitches.clipPlanePitches[0]],
                        flatShading: THREE.SmoothShading
                    });

                    // mesh
                    let mesh = new THREE.Mesh(bufferGeometry, material);
                    mesh.part = 1;
                    mesh.szTop = coordObj.topSZ;
                    mesh.szBot = coordObj.bottomSZ;
                    mesh.szSidePos = sidePos;
                    mesh.szSideLength = sideLength;
                    mesh.inning = halfInning + " " + threeDpitches.inningString(inning);
                    mesh.pitchCount = value['pitchNumber'] + " of " + mapLength;
                    if (isLive && !clearLive) {
                        mesh.pitchCount = value['pitchNumber'] + " of " + totalPitches;
                    }
                    mesh.matchup = value['pitchHand'] + "HP vs " + value['batterSide'] + "HH";
                    mesh.ballsStrikesOuts = "B" + countsArr[0] + "  S" + countsArr[1] + "  O" + countsArr[2];
                    if (!threeDpitches.averagePlot) {
                        if (isComplete) {
                            if (key == mapLength) {
                                mesh.pitchOutcomeGroup = threeDpitches.pitchOutcomes[threeDpitches.finalOutcomeCodes[value['resultCode']]].group;
                            } else {
                                //console.log('outcomeCode: ' + value['outcomeCode']);
                                mesh.pitchOutcomeGroup = threeDpitches.pitchOutcomes[threeDpitches.outcomeCodes[value['outcomeCode']]].group;
                            }
                        } else {
                            mesh.pitchOutcomeGroup = threeDpitches.pitchOutcomes[threeDpitches.outcomeCodes[value['outcomeCode']]].group;
                        }
                    }
                    mesh.guid = curGuid;
                    mesh.pitchType = value['pitchType'];
                    mesh.pitchSpeed = coordObj['startMPH'];
                    if (!threeDpitches.averagePlot) {
                        if (isComplete) {
                            if (key == mapLength) {
                                mesh.outcomeDesc = value.resultDesc;
                            } else {
                                mesh.outcomeDesc = value.outcomeDesc;
                            }
                        } else {
                            mesh.outcomeDesc = value.outcomeDesc;
                        }
                    }
                    mesh.pitchRpm = coordObj['spinRate'];
                    if (!threeDpitches.averagePlot) {
                        if (isComplete) {
                            if (key == mapLength) {
                                mesh.pitchOutcome = threeDpitches.finalOutcomeCodes[value['resultCode']];
                            } else {
                                mesh.pitchOutcome = threeDpitches.outcomeCodes[value['outcomeCode']];
                            }
                        } else {
                            mesh.pitchOutcome = threeDpitches.outcomeCodes[value['outcomeCode']];
                        }
                    }
                    threeDpitches.pitchesGroup.add(mesh);
                    mesh.callback = function () {
                        pitchTypeDiv = $('#pitchTypeData');
                        pitchTypeDiv.text(threeDpitches.pitchTypes[this.pitchType].description);
                        pitchSpeedDiv = $('#pitchSpeedData');
                        pitchSpeedDiv.text(this.pitchSpeed + " mph");
                        pitchRpmDiv = $('#pitchRpmData');
                        pitchRpmDiv.text(this.pitchRpm);
                        pitchCountDiv = $('#pitchCountData');
                        ballsStrikesOutsDiv = $('#ballsStrikesOutsData');
                        inningDiv = $('#inningData');
                        matchupDiv = $('#matchupData');
                        pitchOutcomeDiv = $('#pitchOutcomeData');
                        if (!threeDpitches.averagePlot) {
                            ballsStrikesOutsDiv
                                .text(this.ballsStrikesOuts)
                                .parent()
                                .show();
                            inningDiv
                                .text(this.inning)
                                .parent()
                                .show();
                            matchupDiv
                                .text(this.matchup)
                                .parent()
                                .show();
                            pitchCountDiv
                                .text(this.pitchCount)
                                .parent()
                                .show();
                            pitchOutcomeDiv
                                .parent()
                                .show();
                            if (key == mapLength) {
                                pitchOutcomeDiv.text(value.resultCode);
                            } else {
                                pitchOutcomeDiv.text(value.outcomeDesc);
                            }
                        } else {
                            ballsStrikesOutsDiv
                                .parent()
                                .hide();
                            inningDiv
                                .parent()
                                .hide();
                            matchupDiv
                                .parent()
                                .hide();
                            pitchCountDiv
                                .parent()
                                .hide();
                            pitchOutcomeDiv
                                .parent()
                                .hide();
                        }
                        threeDpitches.szTempSide = this.szSidePos;
                        threeDpitches.szTempTop = this.szTop;
                        threeDpitches.szTempBot = this.szBot;
                        threeDpitches.szSideLengthTemp = this.szSideLength;
                        threeDpitches.addTempStrikeZone();
                    };

                    let curvePoints = path.getPoints(5);

                    let cylMaterial = new THREE.MeshLambertMaterial({
                        color: color,
                        side: THREE.DoubleSide,
                        opacity: 1.0,
                        transparent: true,
                        clippingPlanes: [threeDpitches.clipPlanePitches[0]],
                    });
                    
                    let cylHelper = new THREE.Mesh(cylinderGeometry, cylMaterial);
                    if (!threeDpitches.averagePlot) {
                        if (isComplete) {
                            if (key == mapLength) {
                                cylHelper.pitchOutcomeGroup = threeDpitches.pitchOutcomes[threeDpitches.finalOutcomeCodes[value['resultCode']]].group;
                            } else {
                                cylHelper.pitchOutcomeGroup = threeDpitches.pitchOutcomes[threeDpitches.outcomeCodes[value['outcomeCode']]].group;
                            }
                        } else {
                            cylHelper.pitchOutcomeGroup = threeDpitches.pitchOutcomes[threeDpitches.outcomeCodes[value['outcomeCode']]].group;
                        }
                    }
                    cylHelper.guid = curGuid;
                    cylHelper.part = 2;
                    cylHelper.szTop = coordObj.topSZ;
                    cylHelper.szBot = coordObj.bottomSZ;
                    cylHelper.szSidePos = sidePos;
                    cylHelper.szSideLength = sideLength;
                    cylHelper.pitchCount = key + " of " + mapLength;
                    if (isLive && !clearLive) {
                        cylHelper.pitchCount = value['pitchNumber'] + " of " + totalPitches;
                    }
                    cylHelper.pitchType = value['pitchType'];
                    cylHelper.pitchSpeed = coordObj['startMPH'];
                    cylHelper.pitchRpm = coordObj['spinRate'];
                    cylHelper.inning = halfInning + " " + threeDpitches.inningString(inning);
                    cylHelper.matchup = value['pitchHand'] + "HP vs " + value['batterSide'] + "HH";
                    cylHelper.ballsStrikesOuts = "B" + countsArr[0] + "  S" + countsArr[1] + "  O" + countsArr[2];
                    if (!threeDpitches.averagePlot) {
                        if (isComplete) {
                            if (key == mapLength) {
                                cylHelper.outcomeDesc = value.resultDesc;
                            } else {
                                cylHelper.outcomeDesc = value.outcomeDesc;
                            }
                        } else {
                            cylHelper.outcomeDesc = value.outcomeDesc;
                        }
                        if (isComplete) {
                            if (key == mapLength) {
                                cylHelper.pitchOutcome = threeDpitches.finalOutcomeCodes[value['resultCode']];
                            } else {
                                cylHelper.pitchOutcome = threeDpitches.outcomeCodes[value['outcomeCode']];
                            }
                        } else {
                            cylHelper.pitchOutcome = threeDpitches.outcomeCodes[value['outcomeCode']];
                        }
                    }
                    
                    cylHelper.callback = function () {
                        pitchTypeDiv = $('#pitchTypeData');
                        pitchTypeDiv.text(threeDpitches.pitchTypes[this.pitchType].description);
                        pitchSpeedDiv = $('#pitchSpeedData');
                        pitchSpeedDiv.text(this.pitchSpeed + " mph");
                        pitchRpmDiv = $('#pitchRpmData');
                        pitchRpmDiv.text(this.pitchRpm);
                        pitchCountDiv = $('#pitchCountData');
                        ballsStrikesOutsDiv = $('#ballsStrikesOutsData');
                        inningDiv = $('#inningData');
                        matchupDiv = $('#matchupData');
                        pitchOutcomeDiv = $('#pitchOutcomeData');
                        if (!threeDpitches.averagePlot) {
                            ballsStrikesOutsDiv
                                .text(this.ballsStrikesOuts)
                                .parent()
                                .show();
                            inningDiv
                                .text(this.inning)
                                .parent()
                                .show();
                            matchupDiv
                                .text(this.matchup)
                                .parent()
                                .show();
                            pitchCountDiv
                                .text(this.pitchCount)
                                .parent()
                                .show();
                            pitchOutcomeDiv
                                .parent()
                                .show();
                            if (key == mapLength) {
                                pitchOutcomeDiv.text(value.resultCode);
                            } else {
                                pitchOutcomeDiv.text(value.outcomeDesc);
                            }
                        } else {
                            ballsStrikesOutsDiv
                                .parent()
                                .hide();
                            inningDiv
                                .parent()
                                .hide();
                            matchupDiv
                                .parent()
                                .hide();
                            pitchCountDiv
                                .parent()
                                .hide();
                            pitchOutcomeDiv
                                .parent()
                                .hide();
                        }
                        threeDpitches.szTempSide = this.szSidePos;
                        threeDpitches.szTempTop = this.szTop;
                        threeDpitches.szTempBot = this.szBot;
                        threeDpitches.szSideLengthTemp = this.szSideLength;
                        threeDpitches.addTempStrikeZone();
                    };

                    let ballReleaseMaterial = new THREE.MeshLambertMaterial({
                        color: color,
                        side: THREE.DoubleSide,
                        opacity: 1.0,
                        transparent: true,
                        clippingPlanes: [threeDpitches.clipPlanePitches[0]],
                    });
                    let ballBreakMaterial = new THREE.MeshLambertMaterial({
                        color: color,
                        side: THREE.DoubleSide,
                        opacity: 1.0,
                        transparent: true,
                        clippingPlanes: [threeDpitches.clipPlanePitches[0]],
                    });
                    let ballRelease = new THREE.Mesh(ballGeometry, ballReleaseMaterial);
                    if (!threeDpitches.averagePlot) {
                        if (isComplete) {
                            if (key == mapLength) {
                                ballRelease.pitchOutcomeGroup = threeDpitches.pitchOutcomes[threeDpitches.finalOutcomeCodes[value['resultCode']]].group;
                            } else {
                                ballRelease.pitchOutcomeGroup = threeDpitches.pitchOutcomes[threeDpitches.outcomeCodes[value['outcomeCode']]].group;
                            }
                        } else {
                            ballRelease.pitchOutcomeGroup = threeDpitches.pitchOutcomes[threeDpitches.outcomeCodes[value['outcomeCode']]].group;
                        }
                    }
                    ballRelease.guid = curGuid;
                    ballRelease.part = 3;
                    ballRelease.szTop = coordObj.topSZ;
                    ballRelease.szBot = coordObj.bottomSZ;
                    ballRelease.szSidePos = sidePos;
                    ballRelease.szSideLength = sideLength;
                    ballRelease.pitchCount = key + " of " + mapLength;
                    if (isLive && !clearLive) {
                        ballRelease.pitchCount = value['pitchNumber'] + " of " + totalPitches;
                    }
                    ballRelease.pitchType = value['pitchType'];
                    ballRelease.pitchSpeed = coordObj['startMPH'];
                    ballRelease.pitchRpm = coordObj['spinRate'];
                    ballRelease.inning = halfInning + " " + threeDpitches.inningString(inning);
                    ballRelease.matchup = value['pitchHand'] + "HP vs " + value['batterSide'] + "HH";
                    ballRelease.ballsStrikesOuts = "B" + countsArr[0] + "  S" + countsArr[1] + "  O" + countsArr[2];
                    if (!threeDpitches.averagePlot) {
                        if (isComplete) {
                            if (key == mapLength) {
                                ballRelease.outcomeDesc = value.resultDesc;
                            } else {
                                ballRelease.outcomeDesc = value.outcomeDesc;
                            }
                        } else {
                            ballRelease.outcomeDesc = value.outcomeDesc;
                        }
                        if (isComplete) {
                            if (key == mapLength) {
                                ballRelease.pitchOutcome = threeDpitches.finalOutcomeCodes[value['resultCode']];
                            } else {
                                ballRelease.pitchOutcome = threeDpitches.outcomeCodes[value['outcomeCode']];
                            }
                        } else {
                            ballRelease.pitchOutcome = threeDpitches.outcomeCodes[value['outcomeCode']];
                        }
                    }
                    ballRelease.callback = function () {
                        pitchTypeDiv = $('#pitchTypeData');
                        pitchTypeDiv.text(threeDpitches.pitchTypes[this.pitchType].description);
                        pitchSpeedDiv = $('#pitchSpeedData');
                        pitchSpeedDiv.text(this.pitchSpeed + " mph");
                        pitchRpmDiv = $('#pitchRpmData');
                        pitchRpmDiv.text(this.pitchRpm);
                        pitchCountDiv = $('#pitchCountData');
                        ballsStrikesOutsDiv = $('#ballsStrikesOutsData');
                        inningDiv = $('#inningData');
                        matchupDiv = $('#matchupData');
                        pitchOutcomeDiv = $('#pitchOutcomeData');
                        if (!threeDpitches.averagePlot) {
                            ballsStrikesOutsDiv
                                .text(this.ballsStrikesOuts)
                                .parent()
                                .show();
                            inningDiv
                                .text(this.inning)
                                .parent()
                                .show();
                            matchupDiv
                                .text(this.matchup)
                                .parent()
                                .show();
                            pitchCountDiv
                                .text(this.pitchCount)
                                .parent()
                                .show();
                            pitchOutcomeDiv
                                .parent()
                                .show();
                            if (key == mapLength) {
                                pitchOutcomeDiv.text(value.resultCode);
                            } else {
                                pitchOutcomeDiv.text(value.outcomeDesc);
                            }
                        } else {
                            ballsStrikesOutsDiv
                                .parent()
                                .hide();
                            inningDiv
                                .parent()
                                .hide();
                            matchupDiv
                                .parent()
                                .hide();
                            pitchCountDiv
                                .parent()
                                .hide();
                            pitchOutcomeDiv
                                .parent()
                                .hide();
                        }
                        threeDpitches.szTempSide = this.szSidePos;
                        threeDpitches.szTempTop = this.szTop;
                        threeDpitches.szTempBot = this.szBot;
                        threeDpitches.szSideLengthTemp = this.szSideLength;
                        threeDpitches.addTempStrikeZone();
                    };

                    let ballBreak = new THREE.Mesh(ballGeometry, ballBreakMaterial);
                    if (!threeDpitches.averagePlot) {
                        if (isComplete) {
                            if (key == mapLength) {
                                ballBreak.pitchOutcomeGroup = threeDpitches.pitchOutcomes[threeDpitches.finalOutcomeCodes[value['resultCode']]].group;
                            } else {
                                ballBreak.pitchOutcomeGroup = threeDpitches.pitchOutcomes[threeDpitches.outcomeCodes[value['outcomeCode']]].group;
                            }
                        } else {
                            ballBreak.pitchOutcomeGroup = threeDpitches.pitchOutcomes[threeDpitches.outcomeCodes[value['outcomeCode']]].group;
                        }
                    }
                    ballBreak.guid = curGuid;
                    ballBreak.part = 4;
                    ballBreak.szTop = coordObj.topSZ;
                    ballBreak.szBot = coordObj.bottomSZ;
                    ballBreak.szSidePos = sidePos;
                    ballBreak.szSideLength = sideLength;
                    ballBreak.pitchCount = key + " of " + mapLength;
                    if (isLive && !clearLive) {
                        ballBreak.pitchCount = value['pitchNumber'] + " of " + totalPitches;
                    }
                    ballBreak.pitchType = value['pitchType'];
                    ballBreak.pitchSpeed = coordObj['startMPH'];
                    ballBreak.pitchRpm = coordObj['spinRate'];
                    ballBreak.inning = halfInning + " " + threeDpitches.inningString(inning);
                    ballBreak.matchup = value['pitchHand'] + "HP vs " + value['batterSide'] + "HH";
                    ballBreak.ballsStrikesOuts = "B" + countsArr[0] + "  S" + countsArr[1] + "  O" + countsArr[2];
                    if (!threeDpitches.averagePlot) {
                        if (isComplete) {
                            if (key == mapLength) {
                                ballBreak.outcomeDesc = value.resultDesc;
                            } else {
                                ballBreak.outcomeDesc = value.outcomeDesc;
                            }
                        } else {
                            ballBreak.outcomeDesc = value.outcomeDesc;
                        }
                        if (isComplete) {
                            if (key == mapLength) {
                                ballBreak.pitchOutcome = threeDpitches.finalOutcomeCodes[value['resultCode']];
                            } else {
                                ballBreak.pitchOutcome = threeDpitches.outcomeCodes[value['outcomeCode']];
                            }
                        } else {
                            ballBreak.pitchOutcome = threeDpitches.outcomeCodes[value['outcomeCode']];
                        }
                    }
                    ballBreak.callback = function () {
                        pitchTypeDiv = $('#pitchTypeData');
                        pitchTypeDiv.text(threeDpitches.pitchTypes[this.pitchType].description);
                        pitchSpeedDiv = $('#pitchSpeedData');
                        pitchSpeedDiv.text(this.pitchSpeed + " mph");
                        pitchRpmDiv = $('#pitchRpmData');
                        pitchRpmDiv.text(this.pitchRpm);
                        pitchCountDiv = $('#pitchCountData');
                        ballsStrikesOutsDiv = $('#ballsStrikesOutsData');
                        inningDiv = $('#inningData');
                        matchupDiv = $('#matchupData');
                        pitchOutcomeDiv = $('#pitchOutcomeData');
                        if (!threeDpitches.averagePlot) {
                            ballsStrikesOutsDiv
                                .text(this.ballsStrikesOuts)
                                .parent()
                                .show();
                            inningDiv
                                .text(this.inning)
                                .parent()
                                .show();
                            matchupDiv
                                .text(this.matchup)
                                .parent()
                                .show();
                            pitchCountDiv
                                .text(this.pitchCount)
                                .parent()
                                .show();
                            pitchOutcomeDiv
                                .parent()
                                .show();
                            if (key == mapLength) {
                                pitchOutcomeDiv.text(value.resultCode);
                            } else {
                                pitchOutcomeDiv.text(value.outcomeDesc);
                            }
                        } else {
                            ballsStrikesOutsDiv
                                .parent()
                                .hide();
                            inningDiv
                                .parent()
                                .hide();
                            matchupDiv
                                .parent()
                                .hide();
                            pitchCountDiv
                                .parent()
                                .hide();
                            pitchOutcomeDiv
                                .parent()
                                .hide();
                        }
                        threeDpitches.szTempSide = this.szSidePos;
                        threeDpitches.szTempTop = this.szTop;
                        threeDpitches.szTempBot = this.szBot;
                        threeDpitches.szSideLengthTemp = this.szSideLength;
                        threeDpitches.addTempStrikeZone();
                    };

                    ballBreak.position.set(coordObj['breakPoint'][0], coordObj['breakPoint'][1], coordObj['breakPoint'][2]);
                    threeDpitches.pitchesGroup.add(ballBreak);

                    let countInt = 1;
                    for (let point of curvePoints) {
                        if (countInt === 1) {
                            ballRelease.position.copy(point);
                            threeDpitches.pitchesGroup.add(ballRelease);
                        }
                        if (countInt === curvePoints.length) {
                            cylHelper.position.copy(point);
                            cylHelper.rotation.z = Math.PI;
                            threeDpitches.pitchesGroup.add(cylHelper);
                        }
                        countInt++;
                    }

                    /*
                    let connectingPoints = [];
                    connectingPoints.push(new THREE.Vector3(inputPoints[0][0], inputPoints[0][1], inputPoints[0][2]));
                    connectingPoints.push(new THREE.Vector3(inputPoints[inputPoints.length - 1][0], inputPoints[inputPoints.length - 1][1], inputPoints[inputPoints.length - 1][2]));
                    let connectingPath = new THREE.CatmullRomCurve3(connectingPoints);
                    let connectingGeometry = new THREE.TubeGeometry(connectingPath, 256, 0.01, 32, true);
                    connectingGeometry.mergeVertices();
                    connectingGeometry.computeVertexNormals(true);
                    connectingGeometry.computeFaceNormals();
                    let connectingBufferGeometry = new THREE.BufferGeometry().fromGeometry(connectingGeometry);
                    let connectingMaterial = new THREE.MeshLambertMaterial({
                        color: 'blue',
                        side: THREE.DoubleSide,
                        opacity: 1.0,
                        transparent: true,
                        flatShading: THREE.SmoothShading
                    });
                    let connectingMesh = new THREE.Mesh(connectingBufferGeometry, connectingMaterial);
                    threeDpitches.scene.add(connectingMesh);

                    let tangentPoints = [];
                    tangentPoints.push(new THREE.Vector3(inputPoints[0][0], inputPoints[0][1], inputPoints[0][2]));
                    tangentPoints.push(new THREE.Vector3(this.tangentEnd[0], this.tangentEnd[1], this.tangentEnd[2]));
                    let tangentPath = new THREE.CatmullRomCurve3(tangentPoints);
                    let tangentGeometry = new THREE.TubeGeometry(tangentPath, 256, 0.01, 32, true);
                    tangentGeometry.mergeVertices();
                    tangentGeometry.computeVertexNormals(true);
                    tangentGeometry.computeFaceNormals();
                    let tangentBufferGeometry = new THREE.BufferGeometry().fromGeometry(tangentGeometry);
                    let tangentMaterial = new THREE.MeshLambertMaterial({
                        color: 'red',
                        side: THREE.DoubleSide,
                        opacity: 1.0,
                        transparent: true,
                        flatShading: THREE.SmoothShading
                    });
                    let tangentMesh = new THREE.Mesh(tangentBufferGeometry, tangentMaterial);
                    threeDpitches.scene.add(tangentMesh);
                    */
                }
            });
        }
        await promise;
        if(isInitial){
            threeDpitches.scene.add(threeDpitches.pitchesGroup);
        }
        threeDpitches.setGuids(isInitial, isLive);
    },

    setGuids: async function(isInitial, isLive){
        let promise = new Promise((resolve) => {
            resolve();
        });
        let objects = threeDpitches.pitchesGroup.children;
        let length = objects.length;
        let part, guid;
        for (let i = 0; i < length; i++) {
            part = objects[i].part;
            guid = objects[i].guid;
            switch (part) {
                case 1:
                    let curPitchType = objects[i].pitchType;
                    if (!threeDpitches.curPitchTypes.hasOwnProperty(curPitchType)) {
                        threeDpitches.curPitchTypes[curPitchType] = curPitchType;
                    }
                    threeDpitches.guidMap1[guid] = objects[i];
                    break;
                case 2:
                    threeDpitches.guidMap2[guid] = objects[i];
                    break;
                case 3:
                    threeDpitches.guidMap3[guid] = objects[i];
                    break;
                case 4:
                    threeDpitches.guidMap4[guid] = objects[i];
                    break;
            }
        }
        await promise;
        threeDpitches.addStrikeZone();
        if(isInitial){
            threeDpitches.finalize();
        } else {
            if(isLive){
                threeDpitches.changePitchInfo(false, true);
            } else {
                threeDpitches.changePitchInfo(true, false);
            }
        }
    },

    plotPitches: function(playsArray, isInitial, isAverage, isLive, isComplete, clearLive) {

        let homePlateDim = 1.416666666666667; // Length and width w/o clipped back corners
        let halfHPDim = Number(homePlateDim/2);

        $('#loadingDiv').show();
        threeDpitches.averagePlot = isAverage;
        threeDpitches.playsArray = playsArray;
        if (!isInitial) {
            threeDpitches.processPitches(threeDpitches.playsArray, false, isAverage, isLive, isComplete, clearLive);
        } else {

            let lessHeight = Number(window.innerHeight*.16);
            let newHeight = Number(window.innerHeight - lessHeight);
            let lessWidth = Number(window.innerWidth*.7);
            let newWidth = Number(window.innerWidth - lessWidth);
            // renderer
            threeDpitches.renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
            threeDpitches.renderer.setSize(newWidth, newHeight);
            threeDpitches.renderer.localClippingEnabled = true;
            //renderer.physicallyCorrectLights = true;
            threeDpitches.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
            threeDpitches.renderer.setPixelRatio(window.devicePixelRatio);
            threeDpitches.renderer.setSize(newWidth, newHeight);

            // for texture mapping if needed/desired
            /*
            const textureLoader = new THREE.TextureLoader();
            const textureGrass = textureLoader.load('grass.jpg');
            textureGrass.anisotropy = threeDpitches.renderer.capabilities.getMaxAnisotropy();
            textureGrass.needsUpdate = true;
            const textureDirt = textureLoader.load('dirt.jpeg');
            textureDirt.anisotropy = threeDpitches.renderer.capabilities.getMaxAnisotropy();
            textureDirt.needsUpdate = true;
            const textureBase = textureLoader.load('base.jpg');
            textureBase.anisotropy = threeDpitches.renderer.capabilities.getMaxAnisotropy();
            textureBase.needsUpdate = true;
            const textureChalk = textureLoader.load('chalk.jpg');
            textureChalk.anisotropy = threeDpitches.renderer.capabilities.getMaxAnisotropy();
            textureChalk.needsUpdate = true;
            */

            // for animation progression, stepping through initial to completion
            //let nEnd = 0, nMax, nStep = 90;// 30 faces * 3 vertices/face
            threeDpitches.pitchesGroup = new THREE.Group();
            threeDpitches.mouse = new THREE.Vector2();

            init();

            function init() {

                threeDpitches.yPos = $('#header').height();

                threeDpitches.container = $('<div id="containerDiv" style="position:relative;display:block;"></div>');
                threeDpitches.container.appendTo($('body'));

                // scene
                threeDpitches.scene = new THREE.Scene();

                let measurements = $('<div id="measurements" style="position:fixed; left:1px; bottom:2px; margin-left: 8px; margin-bottom: 5px; display:inline-block; max-width:310px; text-align:left; background-color: #d9d9d9; border: 3px solid rgb(40,40,40);" class="ui-corner-all -moz-selection selection"></div>');
                measurements.hover(function(){
                    measurements.css( 'cursor', 'pointer' );
                });
                let gridsButton = $('<button id="gridsButton" class="-moz-selection selection clicky threeDpitch-font" style="display:inline-block">Grids Off</button>');
                let gridsText = $('<div id="gridsText" class="-moz-selection selection threeDpitch-font" style="display:inline-block; margin-left:10px; font-size:18px; font-weight:bolder">Gridlines in 5ft increments</div>');
                let gridsDiv = $('<div id="grids" state="off" style="position:fixed; left:0px; bottom:5px; display:inline-block" class="-moz-selection selection"></div>');
                gridsDiv.append(gridsButton);
                gridsDiv.append(gridsText);
                let info = $('<div id="pitchInfo" style="position:fixed; bottom:1px; right:1px; min-width:200px; text-align:left; background-color: #d9d9d9; border: 3px solid rgb(40,40,40);" class="-moz-selection selection ui-corner-all ui-widget"></div>');

                let measureHeading = $('<div class="-moz-selection selection threeDpitch-font" style="font-size:16px; display:block; text-align:left; margin: 2px 5px; border-bottom: 2px ridge rgb(40,40,40);width:110px; font-weight:bold">Measurements</div>');
                let pitchInfoHeading = $('<div class="inherit" style="font-weight:bold; display:block; text-align:left; margin-left:5px; margin-top:2px; margin-bottom:5px; border-bottom: 2px ridge rgb(40,40,40);width:120px">Pitcher Info</div>');

                function buildInfoElement() {
                    return $('<div class="inherit" style="display:block; text-align:left; max-width:300px; margin: 2px 5px 5px;"></div>');
                }

                function buildInfoLabel(text) {
                    return $('<div style="display:inline-block; text-align:left; max-width:300px; margin: 2px 2px 5px;">' + text + '</div>');
                }

                function buildLegendLabel(text) {
                    return $('<div class="threeDpitch-font" style="display:block; text-align:left; font-size:12px; margin:2px">' + text + '</div>');
                }

                function buildInfoData(id) {
                    return $('<div id="' + id + '" style="display:inline-block; text-align:left; max-width:300px; margin: 2px 0;"> </div>');
                }

                function buildMeasureElement(){
                    return $('<div class="-moz-selection selection threeDpitch-font" style="display:block; text-align:left; max-width:300px; margin: 0 5px 5px; font-size:10px"></div>');
                }

                function buildMeasureLabel(text){
                    return $('<div class="-moz-selection selection threeDpitch-font" style="display:inline-block; text-align:left; margin:2px 5px 2px 0; font-size:12px; font-weight:bold; vertical-align:top">'+text+'</div>');
                }

                function buildMeasureData(id) {
                    return $('<div id="'+id+'" class="-moz-selection selection threeDpitch-font" style="display:inline-block; text-align:left; max-width:200px; margin:2px 0 2px 0; font-size:12px"> </div>');
                }

                let legendDiv = buildInfoElement();
                let backOfHomePlateDiv = buildMeasureElement();
                let homePlateDiv = buildMeasureElement();
                let mountPlateDiv = buildMeasureElement();
                let moundDiv = buildMeasureElement();
                let basesDimDiv = buildMeasureElement();
                let basesPosDiv = buildMeasureElement();
                let foulDiv = buildMeasureElement();
                let fieldDiv = buildMeasureElement();

                let matchupDiv = buildInfoElement();
                let pitchTypeDiv = buildInfoElement();
                let pitchSpeedDiv = buildInfoElement();
                let pitchRpmDiv = buildInfoElement();
                let pitchCountDiv = buildInfoElement();
                let ballsStrikesOutsDiv = buildInfoElement();
                let inningDiv = buildInfoElement();
                let pitchOutcomeDiv = buildInfoElement();

                legendDiv
                    .append(buildLegendLabel("BOHP = Back of Home Plate"))
                    .append(buildLegendLabel("HP = Home Plate, MP = Mound Plate"));
                backOfHomePlateDiv
                    .append(buildMeasureLabel("BOHP Pos: "))
                    .append(buildMeasureData('backOfHomePlateData'));
                homePlateDiv
                    .append(buildMeasureLabel("HP Dim/Pos: "))
                    .append(buildMeasureData('homePlateData'));
                mountPlateDiv
                    .append(buildMeasureLabel("MP Dim/Pos: "))
                    .append(buildMeasureData('moundPlateData'));
                moundDiv
                    .append(buildMeasureLabel("Mound Dim/Pos: "))
                    .append(buildMeasureData('moundData'));
                basesDimDiv
                    .append(buildMeasureLabel("Bases Dim: "))
                    .append(buildMeasureData('basesDimData'));
                basesPosDiv
                    .append(buildMeasureLabel("Bases Pos: "))
                    .append(buildMeasureData('basesPosData'));
                foulDiv
                    .append(buildMeasureLabel("Foul Poles Pos: "))
                    .append(buildMeasureData('foulPolesData'));
                fieldDiv
                    .append(buildMeasureLabel("Center Field Dim: "))
                    .append(buildMeasureData('centerFieldData'));

                matchupDiv
                    .append(buildInfoLabel("Matchup: "))
                    .append(buildInfoData('matchupData'));
                pitchTypeDiv
                    .append(buildInfoLabel("Type: "))
                    .append(buildInfoData('pitchTypeData'));
                pitchSpeedDiv
                    .append(buildInfoLabel("Speed: "))
                    .append(buildInfoData('pitchSpeedData'));
                pitchRpmDiv
                    .append(buildInfoLabel("RPM: "))
                    .append(buildInfoData('pitchRpmData'));
                pitchCountDiv
                    .append(buildInfoLabel("Pitch: "))
                    .append(buildInfoData('pitchCountData'));
                ballsStrikesOutsDiv
                    .append(buildInfoLabel("Counts: "))
                    .append(buildInfoData('ballsStrikesOutsData'));
                inningDiv
                    .append(buildInfoLabel("Inning: "))
                    .append(buildInfoData('inningData'));
                pitchOutcomeDiv
                    .append(buildInfoLabel("Outcome: "))
                    .append(buildInfoData('pitchOutcomeData'));

                let allMeasure = $('<div id="allMeasurements" style="display:block"></div>');

                allMeasure
                    .append(legendDiv)
                    .append(backOfHomePlateDiv)
                    .append(homePlateDiv)
                    .append(mountPlateDiv)
                    .append(moundDiv)
                    .append(basesDimDiv)
                    .append(basesPosDiv)
                    .append(foulDiv)
                    .append(fieldDiv);

                measurements
                    .append(measureHeading)
                    .append(allMeasure);

                gridsButton.click(function(){
                    gridsText.toggle();
                    let state = gridsDiv.attr("state");
                    if(state === "off"){
                        threeDpitches.reAddGrids();
                        gridsDiv.attr("state", "on");
                        gridsButton.text("Grids On");
                    } else if(state === "on"){
                        threeDpitches.removeGrids();
                        gridsDiv.attr("state", "off");
                        gridsButton.text("Grids Off");
                    }
                });

                measurements.bind('click', function(){
                    allMeasure.toggle();
                    let newWidth = measurements.width();
                    gridsDiv.css("left", Number(newWidth + 20) + "px");
                });

                info
                    .append(pitchInfoHeading)
                    .append(matchupDiv)
                    .append(pitchTypeDiv)
                    .append(pitchSpeedDiv)
                    .append(pitchRpmDiv)
                    .append(pitchCountDiv)
                    .append(ballsStrikesOutsDiv)
                    .append(inningDiv)
                    .append(pitchOutcomeDiv);

                threeDpitches.container.append(measurements);
                threeDpitches.container.append(gridsDiv);
                threeDpitches.container.append(info);
                gridsText.toggle();
                allMeasure.toggle();
                let newWidth = measurements.width();
                gridsDiv.css("left", Number(newWidth + 20) + "px");
                threeDpitches.container.append(threeDpitches.renderer.domElement);

                threeDpitches.camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 600);
                threeDpitches.camera.position.set(6, -15, 7);
                threeDpitches.camera.up.set(0, 0, 1);

                threeDpitches.scene.background = new THREE.Color(0xf0f0f0);
                threeDpitches.scene.add(threeDpitches.camera); //required, since camera has a child light

                // ambient
                threeDpitches.scene.add(new THREE.AmbientLight(0x404040, 1));

                // light
                let light = new THREE.PointLight(0xffffff, 1, 0, 2);
                light.position.set(20, 70, 20);
                light.shadow.mapSize.width = 1024; // default is 512
                light.shadow.mapSize.height = 1024; // default is 512
                threeDpitches.camera.add(light);

                threeDpitches.fieldGroup = new THREE.Object3D();
                threeDpitches.mouse = new THREE.Vector2();

                outfieldGrass();
            }

            function computeVertices(geometry) {
                geometry.mergeVertices();
                geometry.computeVertexNormals(true);
                geometry.computeFaceNormals();
                geometry.computeBoundingBox();
                let max = geometry.boundingBox.max,
                    min = geometry.boundingBox.min;
                let offset = new THREE.Vector2(0 - min.x, 0 - min.y),
                    range = new THREE.Vector2(max.x - min.x, max.y - min.y),
                    faces = geometry.faces;

                geometry.faceVertexUvs[0] = [];

                for (let j = 0; j < faces.length; j++) {

                    let v1 = geometry.vertices[faces[j].a],
                        v2 = geometry.vertices[faces[j].b],
                        v3 = geometry.vertices[faces[j].c];

                    geometry.faceVertexUvs[0].push([
                        new THREE.Vector2((v1.x + offset.x) / range.x, (v1.y + offset.y) / range.y),
                        new THREE.Vector2((v2.x + offset.x) / range.x, (v2.y + offset.y) / range.y),
                        new THREE.Vector2((v3.x + offset.x) / range.x, (v3.y + offset.y) / range.y)
                    ]);
                }
                geometry.uvsNeedUpdate = true;
            }

            function homePlate() {
                $('#backOfHomePlateData').text("x = 0.0, y = -1.416667, z = 0");
                $('#homePlateData').text("17\'l x 17\'w, Two 8.5\" x 8.5\" x 12\" triangles cut @ back");
                let batPlateShape = new THREE.Shape();

                batPlateShape.moveTo(-halfHPDim, 0, 0);
                batPlateShape.lineTo(-halfHPDim, -halfHPDim, 0);
                batPlateShape.lineTo(0, -homePlateDim, 0);
                batPlateShape.lineTo(halfHPDim, -halfHPDim, 0);
                batPlateShape.lineTo(halfHPDim, 0, 0);
                batPlateShape.lineTo(-halfHPDim, 0, 0);
                batPlateShape.lineTo(-halfHPDim, -halfHPDim, 0);
                batPlateShape.lineTo(0, -homePlateDim, 0);
                batPlateShape.lineTo(halfHPDim, -halfHPDim, 0);
                batPlateShape.lineTo(halfHPDim, 0, 0);

                let plateExtrusion = {
                    steps: 2,
                    depth: .1,
                    bevelEnabled: false
                };

                let batPlateGeometry = new THREE.ExtrudeGeometry(batPlateShape, plateExtrusion);
                batPlateGeometry.mergeVertices();
                batPlateGeometry.computeVertexNormals(true);
                batPlateGeometry.computeFaceNormals();
                let batPlateMaterial = new THREE.MeshLambertMaterial({
                    color: 'white',
                    side: THREE.DoubleSide,
                    reflectivity: 0.1,
                    transparent: false
                });
                let batPlateMesh = new THREE.Mesh(batPlateGeometry, batPlateMaterial);

                batPlateMesh.renderOrder = -2;
                batPlateMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };

                threeDpitches.fieldGroup.add(batPlateMesh);

                pitchingMound();
            }

            function batterBoxes() {
                let rhBatterBoxShape = new THREE.Shape();
                let rhBoxRightX = -Number(0.7083333333333333 + 0.5);
                let rhBoxLeftX = -Number(0.7083333333333333 + 4.5);
                let rhBoxTopY = Number(3 - 0.7083333333333333);
                let rhBoxBotY = -Number(3 + 0.7083333333333333);

                let rhInnerBatterBoxShape = new THREE.Shape();
                let rhInnerBoxRightX = -Number(0.7083333333333333 + 0.5 + 0.3333333333333333);
                let rhInnerBoxLeftX = -Number(0.7083333333333333 + 4.5 - 0.3333333333333333);
                let rhInnerBoxTopY = Number(3 - 0.7083333333333333 - 0.3333333333333333);
                let rhInnerBoxBotY = -Number(3 + 0.7083333333333333 - 0.3333333333333333);

                let lhBatterBoxShape = new THREE.Shape();
                let lhBoxRightX = Number(0.7083333333333333 + 0.5);
                let lhBoxLeftX = Number(0.7083333333333333 + 4.5);
                let lhBoxTopY = Number(3 - 0.7083333333333333);
                let lhBoxBotY = -Number(3 + 0.7083333333333333);

                let lhInnerBatterBoxShape = new THREE.Shape();
                let lhInnerBoxLeftX = Number(0.7083333333333333 + 0.5 + 0.3333333333333333);
                let lhInnerBoxRightX = Number(0.7083333333333333 + 4.5 - 0.3333333333333333);
                let lhInnerBoxTopY = Number(3 - 0.7083333333333333 - 0.3333333333333333);
                let lhInnerBoxBotY = -Number(3 + 0.7083333333333333 - 0.3333333333333333);

                rhBatterBoxShape.moveTo(rhBoxRightX, rhBoxTopY, 0);
                rhBatterBoxShape.lineTo(rhBoxLeftX, rhBoxTopY, 0);
                rhBatterBoxShape.lineTo(rhBoxLeftX, rhBoxBotY, 0);
                rhBatterBoxShape.lineTo(rhBoxRightX, rhBoxBotY, 0);
                rhBatterBoxShape.lineTo(rhBoxRightX, rhBoxTopY, 0);

                rhInnerBatterBoxShape.moveTo(rhInnerBoxRightX, rhInnerBoxTopY, 0);
                rhInnerBatterBoxShape.lineTo(rhInnerBoxLeftX, rhInnerBoxTopY, 0);
                rhInnerBatterBoxShape.lineTo(rhInnerBoxLeftX, rhInnerBoxBotY, 0);
                rhInnerBatterBoxShape.lineTo(rhInnerBoxRightX, rhInnerBoxBotY, 0);
                rhInnerBatterBoxShape.lineTo(rhInnerBoxRightX, rhInnerBoxTopY, 0);

                lhBatterBoxShape.moveTo(lhBoxRightX, lhBoxTopY, 0);
                lhBatterBoxShape.lineTo(lhBoxLeftX, lhBoxTopY, 0);
                lhBatterBoxShape.lineTo(lhBoxLeftX, lhBoxBotY, 0);
                lhBatterBoxShape.lineTo(lhBoxRightX, lhBoxBotY, 0);
                lhBatterBoxShape.lineTo(lhBoxRightX, lhBoxTopY, 0);

                lhInnerBatterBoxShape.moveTo(lhInnerBoxRightX, lhInnerBoxTopY, 0);
                lhInnerBatterBoxShape.lineTo(lhInnerBoxLeftX, lhInnerBoxTopY, 0);
                lhInnerBatterBoxShape.lineTo(lhInnerBoxLeftX, lhInnerBoxBotY, 0);
                lhInnerBatterBoxShape.lineTo(lhInnerBoxRightX, lhInnerBoxBotY, 0);
                lhInnerBatterBoxShape.lineTo(lhInnerBoxRightX, lhInnerBoxTopY, 0);

                let rhBatterBoxGeometry = new THREE.ShapeGeometry(rhBatterBoxShape, 24);
                //computeVertices(rhBatterBoxGeometry);

                let rhInnerBatterBoxGeometry = new THREE.ShapeGeometry(rhInnerBatterBoxShape, 24);
                //computeVertices(rhInnerBatterBoxGeometry);


                let lhBatterBoxGeometry = new THREE.ShapeGeometry(lhBatterBoxShape, 24);
                //computeVertices(lhBatterBoxGeometry);


                let lhInnerBatterBoxGeometry = new THREE.ShapeGeometry(lhInnerBatterBoxShape, 24);
                //computeVertices(lhInnerBatterBoxGeometry);

                let batterBoxMaterial = new THREE.MeshLambertMaterial({
                    color: 'white',
                    side: THREE.DoubleSide,
                    reflectivity: 0.1,
                    transparent: false
                });

                let innerBatterBoxMaterial = new THREE.MeshLambertMaterial({
                    color: 'rgb(139,69,19)',
                    side: THREE.DoubleSide,
                    reflectivity: 0.1,
                    transparent: false
                });

                let rhBatterBoxMesh = new THREE.Mesh(rhBatterBoxGeometry, batterBoxMaterial);
                rhBatterBoxMesh.renderOrder = -3;
                rhBatterBoxMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };

                let rhInnerBatterBoxMesh = new THREE.Mesh(rhInnerBatterBoxGeometry, innerBatterBoxMaterial);
                rhInnerBatterBoxMesh.renderOrder = -3;
                rhInnerBatterBoxMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };

                let lhBatterBoxMesh = new THREE.Mesh(lhBatterBoxGeometry, batterBoxMaterial);
                lhBatterBoxMesh.renderOrder = -3;
                lhBatterBoxMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };

                let lhInnerBatterBoxMesh = new THREE.Mesh(lhInnerBatterBoxGeometry, innerBatterBoxMaterial);
                lhInnerBatterBoxMesh.renderOrder = -3;
                lhInnerBatterBoxMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };

                threeDpitches.fieldGroup.add(rhBatterBoxMesh);
                threeDpitches.fieldGroup.add(rhInnerBatterBoxMesh);
                threeDpitches.fieldGroup.add(lhBatterBoxMesh);
                threeDpitches.fieldGroup.add(lhInnerBatterBoxMesh);

                catcherBox();
            }

            function catcherBox() {

                let catcherBoxShape = new THREE.Shape();

                let catcherBoxTopY = -Number(3 + 0.7083333333333333);

                let catcherBoxRightOutX = Number(0.7083333333333333 + 0.5 + 0.5833333333333333 + 0.1666666666666667);
                let catcherBoxLeftOutX = -Number(0.7083333333333333 + 0.5 + 0.5833333333333333 + 0.1666666666666667);
                let catcherBoxBotOutY = -Number(8 + 0.7083333333333333);

                let catcherBoxRightInX = Number(0.7083333333333333 + 0.5 + 0.5833333333333333 + 0.1666666666666667 - 0.3333333333333333);
                let catcherBoxLeftIntX = -Number(0.7083333333333333 + 0.5 + 0.5833333333333333 + 0.1666666666666667 - 0.3333333333333333);
                let catcherBoxBotInY = -Number(8 + 0.7083333333333333 - 0.3333333333333333);

                catcherBoxShape.moveTo(catcherBoxRightOutX, catcherBoxTopY, 0);
                catcherBoxShape.lineTo(catcherBoxRightOutX, catcherBoxBotOutY, 0);
                catcherBoxShape.lineTo(catcherBoxLeftOutX, catcherBoxBotOutY, 0);
                catcherBoxShape.lineTo(catcherBoxLeftOutX, catcherBoxTopY, 0);
                catcherBoxShape.lineTo(catcherBoxLeftIntX, catcherBoxTopY, 0);
                catcherBoxShape.lineTo(catcherBoxLeftIntX, catcherBoxBotInY, 0);
                catcherBoxShape.lineTo(catcherBoxRightInX, catcherBoxBotInY, 0);
                catcherBoxShape.lineTo(catcherBoxRightInX, catcherBoxTopY, 0);
                catcherBoxShape.lineTo(catcherBoxRightOutX, catcherBoxTopY, 0);

                let catcherBoxGeometry = new THREE.ShapeGeometry(catcherBoxShape, 24);
                catcherBoxGeometry.mergeVertices();
                catcherBoxGeometry.computeVertexNormals(true);
                catcherBoxGeometry.computeFaceNormals();

                let catcherBoxMaterial = new THREE.MeshLambertMaterial({
                    color: 'white',
                    side: THREE.DoubleSide,
                    reflectivity: 0.1,
                    transparent: false
                });

                let catcherBoxMesh = new THREE.Mesh(catcherBoxGeometry, catcherBoxMaterial);
                catcherBoxMesh.renderOrder = -3;
                catcherBoxMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };

                threeDpitches.fieldGroup.add(catcherBoxMesh);

                foulLines();
            }

            function foulLines() {
                let leftFieldPoleDist = -230;
                let leftFieldPolePlusFour = Number(leftFieldPoleDist + 0.3333333333333333);
                let rhBoxTopY = Number(3 - 0.7083333333333333);

                let rightFieldPoleDist = 230;
                let rightFieldPolePlusFour = Number(rightFieldPoleDist + 0.3333333333333333);
                let lhBoxTopY = Number(3 - 0.7083333333333333);

                let leftFoulShape = new THREE.Shape();

                let rightFoulShape = new THREE.Shape();

                leftFoulShape.moveTo(-Number(rhBoxTopY + 1.5), rhBoxTopY, 0);
                leftFoulShape.lineTo(-Number(rhBoxTopY + 1.5 - 0.3333333333333333), rhBoxTopY, 0);
                leftFoulShape.lineTo(leftFieldPoleDist, -Number(leftFieldPoleDist + 9.7083333333333333), 0);
                leftFoulShape.lineTo(leftFieldPolePlusFour, -Number(leftFieldPoleDist + 9.7083333333333333 + 0.3333333333333333), 0);
                leftFoulShape.lineTo(-Number(rhBoxTopY + 1.7083333333333333), rhBoxTopY, 0);

                rightFoulShape.moveTo(Number(lhBoxTopY + 1.5), lhBoxTopY, 0);
                rightFoulShape.lineTo(Number(lhBoxTopY + 1.5 - 0.3333333333333333), rhBoxTopY, 0);
                rightFoulShape.lineTo(rightFieldPoleDist, Number(rightFieldPoleDist - 8.7083333333333333), 0);
                rightFoulShape.lineTo(rightFieldPolePlusFour, Number(rightFieldPoleDist - 8.7083333333333333 + 0.3333333333333333), 0);
                rightFoulShape.lineTo(Number(lhBoxTopY + 1.7083333333333333), lhBoxTopY, 0);

                let leftFoulGeometry = new THREE.ShapeGeometry(leftFoulShape, 24);
                leftFoulGeometry.mergeVertices();
                leftFoulGeometry.computeVertexNormals(true);
                leftFoulGeometry.computeFaceNormals();

                let rightFoulGeometry = new THREE.ShapeGeometry(rightFoulShape, 24);
                rightFoulGeometry.mergeVertices();
                rightFoulGeometry.computeVertexNormals(true);
                rightFoulGeometry.computeFaceNormals();

                let foulMaterial = new THREE.MeshLambertMaterial({
                    color: 'white',
                    side: THREE.DoubleSide,
                    reflectivity: 0.1,
                    transparent: false
                });

                let leftFoulMesh = new THREE.Mesh(leftFoulGeometry, foulMaterial);
                leftFoulMesh.renderOrder = -3;
                leftFoulMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };

                let rightFoulMesh = new THREE.Mesh(rightFoulGeometry, foulMaterial);
                rightFoulMesh.renderOrder = -3;
                rightFoulMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };

                threeDpitches.fieldGroup.add(leftFoulMesh);

                threeDpitches.fieldGroup.add(rightFoulMesh);

                foulPoles();
            }

            function foulPoles() {
                $('#foulPolesData').text('327ft from BOHP @ 45deg diagonal');

                let foulPoleGeometry = new THREE.CylinderGeometry( .5, .5, 40, 32 );
                foulPoleGeometry.mergeVertices();
                foulPoleGeometry.computeVertexNormals(true);
                foulPoleGeometry.computeFaceNormals();
                let foulPoleMaterial = new THREE.MeshLambertMaterial({
                    color: 'yellow',
                    side: THREE.DoubleSide,
                    reflectivity:0.3,
                    transparent: false
                });
                let leftFoulPoleCylinder = new THREE.Mesh( foulPoleGeometry, foulPoleMaterial );
                leftFoulPoleCylinder.position.set(-Number(235 - 0.1666666666666667 - 0.5), Number(225.5 - 0.7083333333333333 + 0.1666666666666667), 20);
                leftFoulPoleCylinder.rotation.x = Math.PI / 2;
                let rightFoulPoleCylinder = new THREE.Mesh( foulPoleGeometry, foulPoleMaterial );
                rightFoulPoleCylinder.position.set(Number(235 - 1.1666666666666667), Number(225.5 - 0.7083333333333333 + 0.1666666666666667), 20);
                rightFoulPoleCylinder.rotation.x = Math.PI / 2;
                threeDpitches.fieldGroup.add(leftFoulPoleCylinder);
                threeDpitches.fieldGroup.add(rightFoulPoleCylinder);

                homePlate();
            }

            function bases() {

                $('#basesDimData').text("15\"l x 15\"w square");
                $('#basesPosData').text("1st & 3rd @ 90ft diag from BOHP, 2nd @ 127.28ft from BOHP");
                let baseShape = new THREE.Shape();

                baseShape.moveTo(0, 0, 0);
                baseShape.lineTo(1.25, 0, 0);
                baseShape.lineTo(1.25, 1.25, 0);
                baseShape.lineTo(0, 1.25, 0);
                baseShape.lineTo(0, 0, 0);

                let baseExtrusion = {
                    steps: 2,
                    depth: .2,
                    bevelEnabled: false
                };

                let baseGeometry = new THREE.ExtrudeGeometry(baseShape, baseExtrusion);
                baseGeometry.mergeVertices();
                baseGeometry.computeVertexNormals(true);
                baseGeometry.computeFaceNormals();
                let baseMaterial = new THREE.MeshLambertMaterial({
                    color: 'white',
                    side: THREE.DoubleSide,
                    reflectivity:0.1,
                    transparent: false
                });
                let firstBaseMesh = new THREE.Mesh(baseGeometry, baseMaterial);
                firstBaseMesh.renderOrder = -3;
                firstBaseMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };
                firstBaseMesh.position.set(64.25, 62.75, 0);
                firstBaseMesh.rotation.z = -Math.PI/4;
                threeDpitches.fieldGroup.add(firstBaseMesh);

                let secondBaseMesh = new THREE.Mesh(baseGeometry, baseMaterial);
                secondBaseMesh.renderOrder = -3;
                secondBaseMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };
                secondBaseMesh.position.set(0, 125.24, 0);
                secondBaseMesh.rotation.z = Math.PI/4;
                threeDpitches.fieldGroup.add(secondBaseMesh);

                let thirdBaseMesh = new THREE.Mesh(baseGeometry, baseMaterial);
                thirdBaseMesh.renderOrder = -3;
                thirdBaseMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };
                thirdBaseMesh.position.set(-65.2, 61.7, 0);
                thirdBaseMesh.rotation.z = Math.PI/4;
                threeDpitches.fieldGroup.add(thirdBaseMesh);

                threeDpitches.processPitches(threeDpitches.playsArray, true, threeDpitches.averagePlot, isLive, isComplete, clearLive);
            }

            function battingCircle() {
                let battingShape = new THREE.Shape();

                battingShape.moveTo(10, 0, 0);
                battingShape.absarc(0, 0, 10, 0, 2 * Math.PI);
                let battingGeometry = new THREE.ShapeGeometry(battingShape, 24);
                //computeVertices(battingGeometry);

                let battingMaterial = new THREE.MeshLambertMaterial({
                    color: 'rgb(139,69,19)',
                    side: THREE.DoubleSide,
                    transparent: false,
                    reflectivity: 0.1
                });
                let battingCircle = new THREE.Mesh(battingGeometry, battingMaterial);
                battingCircle.renderOrder = -4;
                battingCircle.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };
                battingCircle.position.copy(new THREE.Vector3(0, 0, 0));
                threeDpitches.fieldGroup.add(battingCircle);

                batterBoxes();
            }

            function pitchingMound() {
                $('#moundData').text("20\'l x 18\'w @ 59\' from BOHP, 1ft rise @ apex");

                let pitchingMoundGeometry = new THREE.SphereGeometry(21, 128, 64, 0, Math.PI * 2, 0, Math.PI);
                pitchingMoundGeometry.mergeVertices();
                pitchingMoundGeometry.computeVertexNormals(true);
                pitchingMoundGeometry.computeFaceNormals();
                threeDpitches.clipPlanes[0].visible = false;
                let pitchingMoundMaterial = new THREE.MeshLambertMaterial({
                    color: 'rgb(139,69,19)',
                    side: THREE.DoubleSide,
                    transparent: false,
                    clippingPlanes: [threeDpitches.clipPlanes[0]],
                    reflectivity: 0.1
                });
                let pitchingMound = new THREE.Mesh(pitchingMoundGeometry, pitchingMoundMaterial);
                pitchingMound.renderOrder = -3;
                pitchingMound.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };
                pitchingMound.position.set(0, 57.5833333, -20);
                threeDpitches.fieldGroup.add(pitchingMound);

                let pitchMoundOutPath = new THREE.Shape();
                pitchMoundOutPath.absellipse(0, 57.5833333, 9, 10, 0, Math.PI * 2, false, 0);
                let pitchMoundOutGeo = new THREE.ShapeGeometry(pitchMoundOutPath, 24);
                pitchMoundOutGeo.mergeVertices();
                pitchMoundOutGeo.computeVertexNormals(true);
                pitchMoundOutGeo.computeFaceNormals();
                let pitchMoundOutMat = new THREE.MeshLambertMaterial({
                    color: 'rgb(139,69,19)',
                    side: THREE.DoubleSide,
                    transparent: false,
                    reflectivity: 0
                });
                let pitchMoundOutEllipse = new THREE.Mesh(pitchMoundOutGeo, pitchMoundOutMat);
                pitchMoundOutEllipse.renderOrder = -4;
                pitchMoundOutEllipse.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };
                threeDpitches.fieldGroup.add(pitchMoundOutEllipse);

                moundPlate();
            }

            function moundPlate() {
                $('#moundPlateData').text("6\"l x 24\"w @ 60.5\' from BOHP, 10in above HP");
                let moundPlateShape = new THREE.Shape();

                moundPlateShape.moveTo(0, 0, 0);
                moundPlateShape.lineTo(2, 0, 0);
                moundPlateShape.lineTo(2, 0.5, 0);
                moundPlateShape.lineTo(0, 0.5, 0);
                moundPlateShape.lineTo(0, 0, 0);

                let moundPlateExtrusion = {
                    steps: 2,
                    depth: .2,
                    bevelEnabled: false
                };

                let moundPlateGeometry = new THREE.ExtrudeGeometry(moundPlateShape, moundPlateExtrusion);
                moundPlateGeometry.mergeVertices();
                moundPlateGeometry.computeVertexNormals(true);
                moundPlateGeometry.computeFaceNormals();
                let moundPlateMaterial = new THREE.MeshLambertMaterial({
                    color: 'white',
                    side: THREE.DoubleSide,
                    transparent: false,
                    reflectivity: 0
                });
                let moundPlateMesh = new THREE.Mesh(moundPlateGeometry, moundPlateMaterial);

                moundPlateMesh.renderOrder = -2;
                moundPlateMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };
                moundPlateMesh.position.set(-1, 59.083333, 0.83);

                threeDpitches.fieldGroup.add(moundPlateMesh);

                /*  Helper to pinpoint correct mound plate placement, relative to back point of home plate
                let moundPlateHelperGeo = new THREE.PlaneGeometry(.1, .1, 12, 12);
                let moundPlateHelperMat = new THREE.MeshLambertMaterial( {
                    color: new THREE.Color('#0000FF'),
                    side: THREE.DoubleSide
                } );
                let moundPlateHelperQuad = new THREE.Mesh( moundPlateHelperGeo, moundPlateHelperMat );
    
                moundPlateHelperQuad.position.set(-1.5, 59.083333, 0.75);
    
                threeDpitches.fieldGroup.add(moundPlateHelperQuad);
                */

                bases();
            }

            function outfieldGrass() {
                $('#centerFieldData').text('400ft from BOHP');

                let outfieldGrassShape = new THREE.Shape();
                outfieldGrassShape.moveTo(0, -20, 0);
                outfieldGrassShape.lineTo(237, 202, 0);
                outfieldGrassShape.absellipse(0, 205, 240, 195, 0, Math.PI);
                outfieldGrassShape.lineTo(0, -20, 0);

                let outfieldGrassGeometry = new THREE.ShapeGeometry(outfieldGrassShape, 24);
                //computeVertices( outfieldGrassGeometry);
                outfieldGrassGeometry.mergeVertices();
                outfieldGrassGeometry.computeVertexNormals(true);
                outfieldGrassGeometry.computeFaceNormals();

                let outfieldGrassMaterial = new THREE.MeshLambertMaterial({
                    color: 'green',
                    side: THREE.DoubleSide,
                    transparent: false,
                    reflectivity: 0.1
                });
                let outfieldGrassMesh = new THREE.Mesh(outfieldGrassGeometry, outfieldGrassMaterial);

                outfieldGrassMesh.renderOrder = -10;
                outfieldGrassMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };

                threeDpitches.fieldGroup.add(outfieldGrassMesh);

                infieldDirt();
            }

            function infieldDirt() {

                let infieldDirtShape = new THREE.Shape();
                infieldDirtShape.moveTo(0, -5, 0);
                infieldDirtShape.lineTo(85, 76, 0);

                infieldDirtShape.absellipse(0, 84.5, 94, 75, 0, Math.PI);
                infieldDirtShape.lineTo(0, -5, 0);

                let infieldDirtGeometry = new THREE.ShapeGeometry(infieldDirtShape, 24);
                //computeVertices( infieldDirtGeometry);

                let infieldDirtMaterial = new THREE.MeshLambertMaterial({
                    color: 'rgb(139,69,19)',
                    side: THREE.DoubleSide,
                    transparent: false,
                    reflectivity: 0.1
                });
                let infieldDirtMesh = new THREE.Mesh(infieldDirtGeometry, infieldDirtMaterial);

                infieldDirtMesh.renderOrder = -9;
                infieldDirtMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };

                threeDpitches.fieldGroup.add(infieldDirtMesh);

                infieldGrass();
            }

            function infieldGrass() {

                let infieldGrassShape = new THREE.Shape();
                infieldGrassShape.moveTo(0, 2, 0);
                infieldGrassShape.lineTo(63.575, 63.575, 0);
                infieldGrassShape.lineTo(0, 125.24, 0);
                infieldGrassShape.lineTo(-63.575, 63.575, 0);
                infieldGrassShape.lineTo(0, 2, 0);

                let infieldGrassGeometry = new THREE.ShapeGeometry(infieldGrassShape, 24);
                //computeVertices(infieldGrassGeometry);

                let infieldGrassMaterial = new THREE.MeshLambertMaterial({
                    color: 'green',
                    side: THREE.DoubleSide,
                    transparent: false,
                    reflectivity: 0.1
                });
                let infieldGrassMesh = new THREE.Mesh(infieldGrassGeometry, infieldGrassMaterial);

                infieldGrassMesh.renderOrder = -8;
                infieldGrassMesh.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };

                threeDpitches.fieldGroup.add(infieldGrassMesh);

                baseCircles();
            }

            function baseCircles() {
                /*
                let clippingHelpers = new THREE.Group();
                clippingHelpers.add(new THREE.PlaneHelper(threeDpitches.clipPlanes[1], 100, 0x00ff00));
                clippingHelpers.add(new THREE.PlaneHelper(threeDpitches.clipPlanes[2], 100, 0x0000ff));
                clippingHelpers.visible = true;
                */
                let baseCircleMaterial = new THREE.MeshLambertMaterial({
                    color: 'rgb(139,69,19)',
                    side: THREE.DoubleSide,
                    transparent: false,
                    reflectivity: 0.1,
                    clippingPlanes: [threeDpitches.clipPlanes[1], threeDpitches.clipPlanes[2]],
                });

                let baseCircleGeometry = new THREE.CircleGeometry(5, 32);
                baseCircleGeometry.mergeVertices();
                baseCircleGeometry.computeVertexNormals(true);
                baseCircleGeometry.computeFaceNormals();

                let baseCircleFirst = new THREE.Mesh(baseCircleGeometry, baseCircleMaterial);
                baseCircleFirst.renderOrder = -7;
                baseCircleFirst.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };
                baseCircleFirst.position.copy(new THREE.Vector3(63.575, 63.575, 0));
                threeDpitches.fieldGroup.add(baseCircleFirst);

                let baseCircleSecond = new THREE.Mesh(baseCircleGeometry, baseCircleMaterial);
                baseCircleSecond.renderOrder = -7;
                baseCircleSecond.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };
                baseCircleSecond.position.copy(new THREE.Vector3(0, 125.24, 0));
                threeDpitches.fieldGroup.add(baseCircleSecond);

                let baseCircleThird = new THREE.Mesh(baseCircleGeometry, baseCircleMaterial);
                baseCircleThird.renderOrder = -7;
                baseCircleThird.onBeforeRender = function (renderer) {
                    renderer.clearDepth();
                };
                baseCircleThird.position.copy(new THREE.Vector3(-63.575, 63.575, 0));
                threeDpitches.fieldGroup.add(baseCircleThird);
                //threeDpitches.fieldGroup.add(clippingHelpers);
                battingCircle();
            }
        }
    },

    createGrid: function(type, start, vertSize, horizSize, step, color) {

        let geometryVert = new THREE.Geometry();
        let geometryHoriz = new THREE.Geometry();
        let material = new THREE.LineDashedMaterial({
            color: color,
            transparent: true,
            opacity: .2
        });

        switch (type){
            case 'xz':
                for ( let i = start; i <= vertSize; i += step) {
                    geometryVert.vertices.push(new THREE.Vector3(-horizSize, 0, i));
                    geometryVert.vertices.push(new THREE.Vector3(horizSize, 0, i));
                    geometryVert.vertices.push(new THREE.Vector3(-horizSize, 0, i));
                }
                for ( let i = -horizSize; i <= horizSize; i += step) {
                    geometryHoriz.vertices.push(new THREE.Vector3( i, 0, start ));
                    geometryHoriz.vertices.push(new THREE.Vector3( i, 0, vertSize ));
                    geometryHoriz.vertices.push(new THREE.Vector3( i, 0, start ));
                }
                break;
            case 'xy':
                for ( let i = start; i <= vertSize; i += step) {
                    geometryVert.vertices.push(new THREE.Vector3(-horizSize, i, 0));
                    geometryVert.vertices.push(new THREE.Vector3(horizSize, i, 0));
                    geometryVert.vertices.push(new THREE.Vector3(-horizSize, i, 0));
                }
                for ( let i = -horizSize; i <= horizSize; i += step) {
                    geometryHoriz.vertices.push(new THREE.Vector3( i, start, 0 ));
                    geometryHoriz.vertices.push(new THREE.Vector3( i, vertSize, 0 ));
                    geometryHoriz.vertices.push(new THREE.Vector3( i, start, 0 ));
                }
                break;
            case 'yz':
                for ( let i = start; i <= vertSize; i += step) {
                    geometryVert.vertices.push(new THREE.Vector3(0, start, i));
                    geometryVert.vertices.push(new THREE.Vector3(0, horizSize, i));
                    geometryVert.vertices.push(new THREE.Vector3(0, start, i));
                }
                for ( let i = start; i <= horizSize; i += step) {
                    geometryHoriz.vertices.push(new THREE.Vector3(0,  i, start));
                    geometryHoriz.vertices.push(new THREE.Vector3(0,  i, vertSize));
                    geometryHoriz.vertices.push(new THREE.Vector3(0,  i, start));
                }
                break;
        }

        return [new THREE.Line(geometryVert, material), new THREE.Line(geometryHoriz, material)];
    },

    addGrids: function(){
        let axesHelper = new THREE.AxesHelper( 150 );
        threeDpitches.gridGroup.add( axesHelper );
        threeDpitches.gridStorage.axes = axesHelper;

        let xyPlanes = threeDpitches.createGrid('xy', -5, 150, 100, 5, 'red');
        let xzPlanes = threeDpitches.createGrid('xz', 0, 50, 100, 5, 'blue');
        let yzPlanes = threeDpitches.createGrid('yz', 0, 50, 150, 5, 'lime');

        threeDpitches.gridGroup.add( xyPlanes[0] );
        threeDpitches.gridGroup.add( xyPlanes[1] );
        threeDpitches.gridStorage.xyPlanes = xyPlanes;
        threeDpitches.gridGroup.add( xzPlanes[0] );
        threeDpitches.gridGroup.add( xzPlanes[1] );
        threeDpitches.gridStorage.xzPlanes = xzPlanes;
        threeDpitches.gridGroup.add( yzPlanes[0] );
        threeDpitches.gridGroup.add( yzPlanes[1] );
        threeDpitches.gridStorage.yzPlanes = yzPlanes;

        threeDpitches.scene.add(threeDpitches.gridGroup);
    },

    finalize: function() {
        threeDpitches.addGrids();

        threeDpitches.scene.add(threeDpitches.fieldGroup);
        threeDpitches.scene.background = new THREE.Color(0xCCCCCC);

        threeDpitches.container.append(threeDpitches.infoSelector());
        threeDpitches.container.append(threeDpitches.viewSelector());
        //$('#viewSelectorList').accordion({ heightStyle: "content", collapsible: true, active: false });
        $('#viewSelectorDiv')
            .hover(function () {
                $('#viewSelectorList').slideDown()
            },function () {
                $('#viewSelectorList').slideUp()
            });
        $('.view-selection').click(function () {
            let selection = this.getAttribute('value');
            //console.log("ViewSelected: " + selection);
            threeDpitches.changeView(selection);
        });
        $('#viewSelectorList').slideUp();
        //$("#pitchInfo").css("visibility", "hidden");

        // controls
        threeDpitches.controls = new THREE.OrbitControls(threeDpitches.camera, threeDpitches.renderer.domElement);
        threeDpitches.controls.maxPolarAngle = (Math.PI * 17) / 32;

        //window.document.addEventListener('mousemove', threeDpitches.onDocumentMouseMove, false);
        //window.addEventListener('resize', threeDpitches.onWindowResize, false);

        threeDpitches.controls.target = new THREE.Vector3(0, 0, 2.5);
        threeDpitches.controls.enabled = false;
        threeDpitches.camera.rotation.z = Math.PI;
        threeDpitches.controls.enabled = true;
        threeDpitches.camera.updateMatrixWorld();
        threeDpitches.controls.update();

        threeDpitches.changePitchInfo(false, false);

        threeDpitches.startAnimation();
        threeDpitches.animating = true;
        threeDpitches.timeout = window.setTimeout(function () {
            threeDpitches.stopAnimation();
        }, 300);
        $("canvas")
            .bind('wheel', function(){threeDpitches.onMouseWheel();})
            .bind('click', function(){
                let allMeasure = $('#allMeasurements');
                if(allMeasure.is(":hidden")){
                } else {
                    allMeasure.toggle();
                    let newWidth = $('#measurements').width();
                    $('#grids').css("left", Number(newWidth + 20) + "px");
                }
            });

        $("#header").bind('click', function(){
            let allMeasure = $('#allMeasurements');
            if(allMeasure.is(":hidden")){
            } else {
                allMeasure.toggle();
                let newWidth = $('#measurements').width();
                $('#grids').css("left", Number(newWidth + 20) + "px");
            }
            threeDpitches.onMouseClick();
        });

        $(window)
            .bind('keydown', function(e){
                let keyID = e.keyCode || e.which || e.key;
                if(threeDpitches.arrowKeysMap.hasOwnProperty(keyID)){
                    e.preventDefault();
                }
                threeDpitches.onKeyDown();
            })
            .bind('keyup', function(){threeDpitches.onKeyUp();})
            .bind('resize', function(){threeDpitches.onWindowResize();})
            .bind('mousemove', function(e){e.preventDefault();threeDpitches.onDocumentMouseMove();});

        window.onbeforeunload = threeDpitches.cleanUp();
    },

    stopAnimation: async function(){
        if(threeDpitches.animating) {
            let animationComplete = new Promise((resolve) => {
                resolve();
            });
            cancelAnimationFrame(threeDpitches.animationFrame);
            await(animationComplete);
            threeDpitches.animating = false;
        }
    },

    onWindowResize: async function() {
        if(!threeDpitches.moving || threeDpitches.timeout === undefined) {
            threeDpitches.moving = true;
            let movingComplete = new Promise((resolve) => {
                resolve();
            });
            if (!threeDpitches.animating) {
                threeDpitches.animating = true;
                threeDpitches.startAnimation();
            }
            if (threeDpitches.timeout !== undefined) {
                window.clearTimeout(threeDpitches.timeout);
            }
            threeDpitches.timeout = window.setTimeout(function () {
                threeDpitches.stopAnimation();
            }, 300);
            await(movingComplete);
            threeDpitches.moving = false;
        }
        let lessHeight = Number(window.innerHeight*.16);
        let newHeight = Number(window.innerHeight - lessHeight);
        let lessWidth = Number(window.innerWidth*.7);
        let newWidth = Number(window.innerWidth - lessWidth);
        threeDpitches.camera.aspect = newWidth / newHeight;
        threeDpitches.camera.left = -40 * threeDpitches.camera.aspect / 2;
        threeDpitches.camera.right = 40 * threeDpitches.camera.aspect / 2;
        threeDpitches.camera.top = 40 / 2;
        threeDpitches.camera.bottom = -40 / 2;
        threeDpitches.renderer.setSize(newWidth, newHeight);
        threeDpitches.camera.updateProjectionMatrix();
        threeDpitches.controls.update();
    },

    onMouseWheel: async function(){
        if(!threeDpitches.moving || threeDpitches.timeout === undefined){
            threeDpitches.moving = true;
            let movingComplete = new Promise((resolve) => {
                resolve();
            });
            if(!threeDpitches.animating) {
                threeDpitches.animating = true;
                threeDpitches.startAnimation();
            }
            if (threeDpitches.timeout !== undefined) {
                window.clearTimeout(threeDpitches.timeout);
            }
            threeDpitches.timeout = window.setTimeout(function () {
                threeDpitches.stopAnimation();
            }, 300);
            await(movingComplete);
            threeDpitches.moving = false;
        }
    },

    onMouseClick: async function(){
        if(!threeDpitches.moving || threeDpitches.timeout === undefined){
            threeDpitches.moving = true;
            let movingComplete = new Promise((resolve) => {
                resolve();
            });
            if(!threeDpitches.animating) {
                threeDpitches.animating = true;
                threeDpitches.startAnimation();
            }
            if (threeDpitches.timeout !== undefined) {
                window.clearTimeout(threeDpitches.timeout);
            }
            threeDpitches.timeout = window.setTimeout(function () {
                threeDpitches.stopAnimation();
            }, 300);
            await(movingComplete);
            threeDpitches.moving = false;
        }
    },

    onKeyUp: async function(){
        let movingComplete = new Promise((resolve) => {
            resolve();
        });
        if (threeDpitches.timeout !== undefined) {
            window.clearTimeout(threeDpitches.timeout);
        }
        threeDpitches.timeout = window.setTimeout(function () {
            threeDpitches.stopAnimation();
        }, 300);
        await(movingComplete);
        threeDpitches.moving = false;
    },

    onKeyDown: async function(){
        if (threeDpitches.timeout !== undefined) {
            window.clearTimeout(threeDpitches.timeout);
        }
        if(!threeDpitches.moving || threeDpitches.timeout === undefined){
            threeDpitches.moving = true;
            if(!threeDpitches.animating) {
                threeDpitches.animating = true;
                threeDpitches.startAnimation();
            }
        }
    },

    onDocumentMouseMove: async function() {
        if(!threeDpitches.moving || threeDpitches.timeout === undefined) {
            threeDpitches.moving = true;
            let movingComplete = new Promise((resolve) => {
                resolve();
            });
            if (!threeDpitches.animating) {
                threeDpitches.animating = true;
                threeDpitches.startAnimation();
            }
            if (threeDpitches.timeout !== undefined) {
                window.clearTimeout(threeDpitches.timeout);
            }
            threeDpitches.timeout = window.setTimeout(function () {
                threeDpitches.stopAnimation();
            }, 300);
            await(movingComplete);
            threeDpitches.moving = false;
        }
        threeDpitches.rect = threeDpitches.renderer.domElement.getBoundingClientRect();
        threeDpitches.mouse.x = ((event.clientX - threeDpitches.rect.left) / (threeDpitches.rect.right - threeDpitches.rect.left)) * 2 - 1;
        threeDpitches.mouse.y = -((event.clientY - threeDpitches.rect.top) / (threeDpitches.rect.bottom - threeDpitches.rect.top)) * 2 + 1;
    },

    infoSelector: function() {
        let infoSelectorDiv = $('<div id="pitchInfoSelectorDiv" class="ui-corner-all ui-widget" style="position:absolute; top:8px; left:8px; margin-bottom: 5px; width:170px; text-align:left; background-color:#d9d9d9; z-index:9999; border: 2px ridge rgb(40,40,40);"></div>');

        let arr = [
            {val: 1, text: 'Pitch Types'},
            {val: 2, text: 'Speed'},
            {val: 3, text: 'Spin'},
            {val: 4, text: 'Outcome'}
        ];

        let pitchInfoSelector = $("<select id='pitchInfoSelector' class='ui-corner-all ui-widget-content' style='z-index: 10000; display:block; margin-top:5px; margin-left:5px;'>").appendTo(infoSelectorDiv);
        $(arr).each(function () {
            pitchInfoSelector.append($("<option id='info-option-"+ this.text +"'>").attr('value', this.val).text(this.text));
        });

        pitchInfoSelector.change(function () {
            threeDpitches.changePitchInfo(false, false);
        });

        let pitchInfoSelectorList = $("<ul id='pitchInfoSelectorList' class='ui-corner-all' style='list-style-type: none; padding: 0; margin-top: 5px; margin-left: 5px; width: 160px;'>").appendTo(infoSelectorDiv);

        return infoSelectorDiv;
    },

    viewSelector: function() {
        let arr = [
            {val: 1, text: 'Umpire'},
            {val: 2, text: 'Pitcher'},
            {val: 3, text: 'Catcher'},
            {val: 4, text: 'LHH'},
            {val: 5, text: 'RHH'},
            {val: 6, text: 'Overhead'},
            {val: 7, text: '1st Base Side'},
            {val: 8, text: '3rd Base Side'},
            {val: 9, text: 'Zone'}
        ];

        let viewSelectorDiv = $('<div id="viewSelectorDiv" class="ui-corner-all ui-widget" ' +
            'style="position:absolute; top:8px; right:1px; margin-bottom:5px; width:112px; text-align:center; background-color:#d9d9d9; z-index:10000; border: 2px ridge rgb(40,40,40);white-space:nowrap; font-weight:bolder">View</div>');

        let viewSelectorList = $("<ol id='viewSelectorList' style='horiz-align: center; padding: 0; list-style-type: none; display:block; width: 100px; margin: 3px 5px;background-color: inherit;'>").appendTo(viewSelectorDiv);
        $(arr).each(function () {
            viewSelectorList.append($("<li style='white-space:nowrap; height: 16px; vertical-align: center; font-size:12px; width: 100px; background-color: #eaeaea;' class='ui-corner-all ui-widget-content view-selection'>").attr('value', this.val).text(this.text).on('mouseenter', function () {
                this.style.cursor = 'pointer';
                this.style.backgroundColor = '#4ca2cd'
            }).on('mouseleave', function () {
                this.style.cursor = 'auto';
                this.style.backgroundColor = '#eaeaea';
            }));
        });

        return viewSelectorDiv;
    },

    startAnimation: function() {

        threeDpitches.animationFrame = requestAnimationFrame(threeDpitches.startAnimation);

        threeDpitches.render();

        //cancelAnimationFrame(animation);

    },

    postAnimate: function() {
        requestAnimationFrame( threeDpitches.postAnimate );
        threeDpitches.renderer.render( threeDpitches.scene, threeDpitches.camera );
        threeDpitches.camera.updateMatrixWorld();
        threeDpitches.controls.update();
    },

    changeView: function(selection) {
        let objects = threeDpitches.pitchesGroup.children;
        let strikeZoneObjects = threeDpitches.strikeZoneGroup.children;
        let message = "";
        switch (selection) {
            case '1':
                message = "You Selected: Umpire View";
                for (let i = 0; i < objects.length; i++) {
                    let guid = objects[i].guid;
                    threeDpitches.guidMap1[guid].material.opacity = 1.0;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].scale.x = 1;
                    threeDpitches.guidMap3[guid].scale.y = 1;
                    threeDpitches.guidMap3[guid].scale.z = 1;
                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].scale.x = 1;
                    threeDpitches.guidMap4[guid].scale.y = 1;
                    threeDpitches.guidMap4[guid].scale.z = 1;
                }
                threeDpitches.camera.position.set(0, -12, 4);
                threeDpitches.controls.maxPolarAngle = (Math.PI*65.5)/128;
                for (let i = 0; i < strikeZoneObjects.length; i++) {
                    strikeZoneObjects[i].material.visible = true;
                }
                threeDpitches.curView = "UMP";
                break;
            case '2':
                message = "You Selected: Pitcher View";
                for (let i = 0; i < objects.length; i++) {
                    let guid = objects[i].guid;
                    threeDpitches.guidMap1[guid].material.opacity = 0.1;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].scale.x = 2;
                    threeDpitches.guidMap3[guid].scale.y = 2;
                    threeDpitches.guidMap3[guid].scale.z = 2;
                    threeDpitches.guidMap4[guid].material.opacity = 0.1;
                    threeDpitches.guidMap4[guid].scale.x = 1;
                    threeDpitches.guidMap4[guid].scale.y = 1;
                    threeDpitches.guidMap4[guid].scale.z = 1;
                }
                for (let i = 0; i < strikeZoneObjects.length; i++) {
                    strikeZoneObjects[i].material.visible = true;
                }
                threeDpitches.camera.position.set(-1, 59.083333, 5);
                threeDpitches.controls.maxPolarAngle = (Math.PI*66)/128;
                threeDpitches.controls.target = new THREE.Vector3(0, 0, 3);
                threeDpitches.curView = "PIT";
                break;
            case '3':
                message = "You Selected: Catcher View";
                for (let i = 0; i < objects.length; i++) {
                    let guid = objects[i].guid;
                    threeDpitches.guidMap1[guid].material.opacity = 1.0;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].scale.x = 1;
                    threeDpitches.guidMap3[guid].scale.y = 1;
                    threeDpitches.guidMap3[guid].scale.z = 1;
                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].scale.x = 1;
                    threeDpitches.guidMap4[guid].scale.y = 1;
                    threeDpitches.guidMap4[guid].scale.z = 1;
                }
                for (let i = 0; i < strikeZoneObjects.length; i++) {
                    strikeZoneObjects[i].material.visible = true;
                }
                threeDpitches.camera.position.set(0, -8, 2);
                threeDpitches.controls.target = new THREE.Vector3(0, 0, 2.5);
                threeDpitches.controls.maxPolarAngle = (Math.PI*65)/128;
                threeDpitches.curView = "CAT";
                break;
            case '4':
                message = "You Selected: LHH View";
                for (let i = 0; i < objects.length; i++) {
                    let guid = objects[i].guid;
                    threeDpitches.guidMap1[guid].material.opacity = 0.1;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].scale.x = 2;
                    threeDpitches.guidMap3[guid].scale.y = 2;
                    threeDpitches.guidMap3[guid].scale.z = 2;
                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].scale.x = 2;
                    threeDpitches.guidMap4[guid].scale.y = 2;
                    threeDpitches.guidMap4[guid].scale.z = 2;
                }
                for (let i = 0; i < strikeZoneObjects.length; i++) {
                    strikeZoneObjects[i].material.visible = false;
                }
                threeDpitches.camera.position.set(3, -4, 5.75);
                threeDpitches.controls.target = new THREE.Vector3(0, 55, 5);
                threeDpitches.controls.maxPolarAngle = (Math.PI*66)/128;
                threeDpitches.curView = "LHH";
                break;
            case '5':
                message = "You Selected: RHH View";
                for (let i = 0; i < objects.length; i++) {
                    let guid = objects[i].guid;
                    threeDpitches.guidMap1[guid].material.opacity = 0.1;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].scale.x = 2;
                    threeDpitches.guidMap3[guid].scale.y = 2;
                    threeDpitches.guidMap3[guid].scale.z = 2;
                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].scale.x = 2;
                    threeDpitches.guidMap4[guid].scale.y = 2;
                    threeDpitches.guidMap4[guid].scale.z = 2;
                }
                for (let i = 0; i < strikeZoneObjects.length; i++) {
                    strikeZoneObjects[i].material.visible = false;
                }
                threeDpitches.camera.position.set(-3, -4, 5.75);
                threeDpitches.controls.target = new THREE.Vector3(0, 55, 5);
                threeDpitches.controls.maxPolarAngle = (Math.PI*66)/128;
                threeDpitches.curView = "RHH";
                break;
            case '6':
                message = "You Selected: Overhead View";
                for (let i = 0; i < objects.length; i++) {
                    let guid = objects[i].guid;
                    threeDpitches.guidMap1[guid].material.opacity = 1.0;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].scale.x = 1;
                    threeDpitches.guidMap3[guid].scale.y = 1;
                    threeDpitches.guidMap3[guid].scale.z = 1;
                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].scale.x = 1;
                    threeDpitches.guidMap4[guid].scale.y = 1;
                    threeDpitches.guidMap4[guid].scale.z = 1;
                }
                for (let i = 0; i < strikeZoneObjects.length; i++) {
                    strikeZoneObjects[i].material.visible = true;
                }
                threeDpitches.camera.position.set(0, 25, 55);
                threeDpitches.controls.target = new THREE.Vector3(0, 25, 7);
                threeDpitches.controls.maxPolarAngle = (Math.PI*66.5)/128;
                threeDpitches.curView = "OHD";
                break;
            case '7':
                message = "You Selected: 1st Base Side View";
                for (let i = 0; i < objects.length; i++) {
                    let guid = objects[i].guid;
                    threeDpitches.guidMap1[guid].material.opacity = 1.0;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].scale.x = 1;
                    threeDpitches.guidMap3[guid].scale.y = 1;
                    threeDpitches.guidMap3[guid].scale.z = 1;
                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].scale.x = 1;
                    threeDpitches.guidMap4[guid].scale.y = 1;
                    threeDpitches.guidMap4[guid].scale.z = 1;
                }
                for (let i = 0; i < strikeZoneObjects.length; i++) {
                    strikeZoneObjects[i].material.visible = true;
                }
                threeDpitches.camera.position.set(63, 62.25, 5);
                threeDpitches.controls.target = new THREE.Vector3(0, 0, 3);
                threeDpitches.controls.maxPolarAngle = (Math.PI*65)/128;
                threeDpitches.curView = "1ST";
                break;
            case '8':
                message = "You Selected: 3rd Base Side View";
                for (let i = 0; i < objects.length; i++) {
                    let guid = objects[i].guid;
                    threeDpitches.guidMap1[guid].material.opacity = 1.0;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].scale.x = 1;
                    threeDpitches.guidMap3[guid].scale.y = 1;
                    threeDpitches.guidMap3[guid].scale.z = 1;
                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].scale.x = 1;
                    threeDpitches.guidMap4[guid].scale.y = 1;
                    threeDpitches.guidMap4[guid].scale.z = 1;
                }
                for (let i = 0; i < strikeZoneObjects.length; i++) {
                    strikeZoneObjects[i].material.visible = true;
                }
                threeDpitches.camera.position.set(-63, 62.25, 5);
                threeDpitches.controls.target = new THREE.Vector3(0, 0, 3);
                threeDpitches.controls.maxPolarAngle = (Math.PI*65)/128;
                threeDpitches.curView = "3RD";
                break;
            case '9':
                message = "You Selected: Zone View";
                for (let i = 0; i < objects.length; i++) {
                    let guid = objects[i].guid;
                    threeDpitches.guidMap1[guid].material.opacity = 1.0;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].scale.x = 1;
                    threeDpitches.guidMap3[guid].scale.y = 1;
                    threeDpitches.guidMap3[guid].scale.z = 1;
                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].scale.x = 1;
                    threeDpitches.guidMap4[guid].scale.y = 1;
                    threeDpitches.guidMap4[guid].scale.z = 1;
                }
                for (let i = 0; i < strikeZoneObjects.length; i++) {
                    strikeZoneObjects[i].material.visible = true;
                }
                threeDpitches.camera.position.set(0, -3, 2.5);
                threeDpitches.controls.target = new THREE.Vector3(0, 0, 2.5);
                threeDpitches.controls.maxPolarAngle = (Math.PI*65)/128;
                threeDpitches.curView = "ZON";
                break;
            default:
                message = "Invalid Selection";
                break;
        }
        threeDpitches.camera.updateMatrixWorld();
        threeDpitches.controls.update();
    },
    
    changePitchInfo: async function(redraw, isLive) {
        if (threeDpitches.averagePlot) {
            $('#info-option-Outcome').hide();
        } else {
            $('#info-option-Outcome').show();
        }
        let promise = new Promise((resolve) => {
            resolve();
        });
        if(redraw){
            $('#pitchInfoSelector').prop('selectedIndex',0);
        }
        let selection = $("#pitchInfoSelector option:selected").val();

        let objects = threeDpitches.pitchesGroup.children;
        let pitchType, pitchSpeed, pitchRpm, pitchOutcome, guid;
        let pitchInfoSelectorList = $('#pitchInfoSelectorList');
        switch (selection) {
            case '1':

                for (let i = 0; i < objects.length; i++) {
                    pitchType = objects[i].pitchType;
                    objects[i].material.color = new THREE.Color(threeDpitches.pitchTypeColor(pitchType));
                    guid = objects[i].guid;
                    threeDpitches.guidMap1[guid].visible = true;
                    threeDpitches.guidMap2[guid].visible = true;
                    threeDpitches.guidMap3[guid].visible = true;
                    threeDpitches.guidMap4[guid].visible = true;
                    if (threeDpitches.curView === 'PIT') {
                        threeDpitches.guidMap1[guid].material.opacity = 0.1;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 0.1;
                    } else if (threeDpitches.curView === 'LHH' || threeDpitches.curView === 'RHH') {
                        threeDpitches.guidMap1[guid].material.opacity = 0.1;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    } else {
                        threeDpitches.guidMap1[guid].material.opacity = 1.0;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    }
                }
                pitchInfoSelectorList.empty();
                $.each(threeDpitches.pitchTypes, function (key, value) {
                    if(threeDpitches.curPitchTypes.hasOwnProperty(key)){
                        let description = value['description'];
                        let color = value['color'];
                        let listItem = $("<li id='" + key + "' style='height:25px;'></li>").appendTo(pitchInfoSelectorList);
                        $("<div style='vertical-align: middle; white-space:nowrap; margin-right: 15px; font-size:12px; display:inline-block; width: 10px; height: 10px; background-color: " + color + ";margin-top:5px; margin-left:5px;'></div><p style='vertical-align: middle; font-size:12px; display:inline-block; margin-top: 0px; margin-bottom: 0px;'>" + description + "</p>").appendTo(listItem);
                        listItem.on('mouseenter', function () {
                            this.style.backgroundColor = '#4ca2cd';
                            this.style.cursor = 'pointer';
                            for (let i = 0; i < objects.length; i++) {
                                let pitchType = objects[i].pitchType;
                                if (pitchType !== this.id) {
                                    let guid = objects[i].guid;
                                    threeDpitches.guidMap1[guid].visible = false;
                                    threeDpitches.guidMap2[guid].visible = false;
                                    threeDpitches.guidMap3[guid].visible = false;
                                    threeDpitches.guidMap4[guid].visible = false;
                                } else {
                                }
                            }
                        });
                        listItem.on('mouseleave', function () {
                            this.style.cursor = 'auto';
                            this.style.backgroundColor = '#d9d9d9';
                            for (let i = 0; i < objects.length; i++) {
                                let guid = objects[i].guid;
                                threeDpitches.guidMap1[guid].visible = true;
                                threeDpitches.guidMap2[guid].visible = true;
                                threeDpitches.guidMap3[guid].visible = true;
                                threeDpitches.guidMap4[guid].visible = true;
                                if (threeDpitches.curView === 'PIT') {
                                    threeDpitches.guidMap1[guid].material.opacity = 0.1;
                                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap4[guid].material.opacity = 0.1;
                                } else if (threeDpitches.curView === 'LHH' || threeDpitches.curView === 'RHH') {
                                    threeDpitches.guidMap1[guid].material.opacity = 0.1;
                                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                                } else {
                                    threeDpitches.guidMap1[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                                }
                            }
                        });
                    }
                });
                break;
            case '2':
                for (let i = 0; i < objects.length; i++) {
                    pitchSpeed = objects[i].pitchSpeed;
                    objects[i].material.color = new THREE.Color(threeDpitches.pitchSpeedColor(pitchSpeed));
                    guid = objects[i].guid;
                    threeDpitches.guidMap1[guid].visible = true;
                    threeDpitches.guidMap2[guid].visible = true;
                    threeDpitches.guidMap3[guid].visible = true;
                    threeDpitches.guidMap4[guid].visible = true;
                    if (threeDpitches.curView === 'PIT') {
                        threeDpitches.guidMap1[guid].material.opacity = 0.1;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 0.1;
                    } else if (threeDpitches.curView === 'LHH' || threeDpitches.curView === 'RHH') {
                        threeDpitches.guidMap1[guid].material.opacity = 0.1;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    } else {
                        threeDpitches.guidMap1[guid].material.opacity = 1.0;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    }
                    pitchInfoSelectorList.empty();
                    $.each(threeDpitches.pitchSpeeds, function (key, value) {
                        let description = value['description'];
                        let color = value['color'];
                        let listItem = $("<li id='" + key + "' style='height:25px;'></li>").appendTo(pitchInfoSelectorList);
                        $("<div style='vertical-align: middle; white-space:nowrap; margin-right: 15px; font-size:12px; display:inline-block; width: 10px; height: 10px; background-color: " + color + ";margin-top:5px; margin-left:5px;'></div><p style='vertical-align: middle; font-size:12px; display:inline-block; margin-top: 0px; margin-bottom: 0px;'>" + description + "</p>").appendTo(listItem);
                        listItem.on('mouseenter', function () {
                            //console.log("This.Id: " + this.id);
                            this.style.cursor = 'pointer';
                            this.style.backgroundColor = '#4ca2cd';
                            for (let i = 0; i < objects.length; i++) {
                                let pitchSpeed = objects[i].pitchSpeed;
                                switch (this.id) {
                                    case '70':
                                        if (pitchSpeed >= 70) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                    case '75':
                                        if (pitchSpeed >= 75 || pitchSpeed < 70) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                    case '80':
                                        if (pitchSpeed >= 80 || pitchSpeed < 75) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                    case '85':
                                        if (pitchSpeed >= 85 || pitchSpeed < 80) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                    case '90':
                                        if (pitchSpeed >= 90 || pitchSpeed < 85) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                    case '95':
                                        if (pitchSpeed >= 95 || pitchSpeed < 90) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                    case '100':
                                        if (pitchSpeed >= 100 || pitchSpeed < 95) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        } else {
                                        }
                                        break;
                                    case '101':
                                        if (pitchSpeed < 100) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                }

                            }
                        });
                        listItem.on('mouseleave', function () {
                            this.style.cursor = 'auto';
                            this.style.backgroundColor = '#d9d9d9';
                            for (let i = 0; i < objects.length; i++) {
                                let guid = objects[i].guid;
                                threeDpitches.guidMap1[guid].visible = true;
                                threeDpitches.guidMap2[guid].visible = true;
                                threeDpitches.guidMap3[guid].visible = true;
                                threeDpitches.guidMap4[guid].visible = true;
                                if (threeDpitches.curView === 'PIT') {
                                    threeDpitches.guidMap1[guid].material.opacity = 0.1;
                                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap4[guid].material.opacity = 0.1;
                                } else if (threeDpitches.curView === 'LHH' || threeDpitches.curView === 'RHH') {
                                    threeDpitches.guidMap1[guid].material.opacity = 0.1;
                                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                                } else {
                                    threeDpitches.guidMap1[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                                }
                            }
                        });
                    });
                }
                break;
            case '3':
                for (let i = 0; i < objects.length; i++) {
                    pitchRpm = objects[i].pitchRpm;
                    objects[i].material.color = new THREE.Color(threeDpitches.pitchRpmColor(pitchRpm));
                    guid = objects[i].guid;
                    threeDpitches.guidMap1[guid].visible = true;
                    threeDpitches.guidMap2[guid].visible = true;
                    threeDpitches.guidMap3[guid].visible = true;
                    threeDpitches.guidMap4[guid].visible = true;
                    if (threeDpitches.curView === 'PIT') {
                        threeDpitches.guidMap1[guid].material.opacity = 0.1;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 0.1;
                    } else if (threeDpitches.curView === 'LHH' || threeDpitches.curView === 'RHH') {
                        threeDpitches.guidMap1[guid].material.opacity = 0.1;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    } else {
                        threeDpitches.guidMap1[guid].material.opacity = 1.0;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    }
                    pitchInfoSelectorList.empty();
                    $.each(threeDpitches.pitchRpms, function (key, value) {
                        let description = value['description'];
                        let color = value['color'];
                        let listItem = $("<li id='" + key + "' style='height:25px;'></li>").appendTo(pitchInfoSelectorList);
                        $("<div style='vertical-align: middle; white-space:nowrap; margin-right: 15px; font-size:12px; display:inline-block; width: 10px; height: 10px; background-color: " + color + ";margin-top:5px; margin-left:5px;'></div><p style='vertical-align: middle; font-size:12px; display:inline-block; margin-top: 0px; margin-bottom: 0px;'>" + description + "</p>").appendTo(listItem);
                        listItem.on('mouseenter', function () {
                            this.style.cursor = 'pointer';
                            this.style.backgroundColor = '#4ca2cd';
                            for (let i = 0; i < objects.length; i++) {
                                let pitchRpm = objects[i].pitchRpm;
                                switch (this.id) {
                                    case '1500':
                                        if (pitchRpm >= 1500) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                    case '1750':
                                        if (pitchRpm >= 1750 || pitchRpm < 1500) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                    case '2000':
                                        if (pitchRpm >= 2000 || pitchRpm < 1750) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                    case '2250':
                                        if (pitchRpm >= 2250 || pitchRpm < 2000) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                    case '2500':
                                        if (pitchRpm >= 2500 || pitchRpm < 2250) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                    case '2750':
                                        if (pitchRpm >= 2750 || pitchRpm < 2500) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                    case '3000':
                                        if (pitchRpm >= 3000 || pitchRpm < 2750) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                    case '3001':
                                        if (pitchRpm < 3000) {
                                            let guid = objects[i].guid;
                                            threeDpitches.guidMap1[guid].visible = false;
                                            threeDpitches.guidMap2[guid].visible = false;
                                            threeDpitches.guidMap3[guid].visible = false;
                                            threeDpitches.guidMap4[guid].visible = false;
                                        }
                                        break;
                                }
                            }
                        });
                        listItem.on('mouseleave', function () {
                            this.style.cursor = 'auto';
                            this.style.backgroundColor = '#d9d9d9';
                            for (let i = 0; i < objects.length; i++) {
                                let guid = objects[i].guid;
                                threeDpitches.guidMap1[guid].visible = true;
                                threeDpitches.guidMap2[guid].visible = true;
                                threeDpitches.guidMap3[guid].visible = true;
                                threeDpitches.guidMap4[guid].visible = true;
                                if (threeDpitches.curView === 'PIT') {
                                    threeDpitches.guidMap1[guid].material.opacity = 0.1;
                                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap4[guid].material.opacity = 0.1;
                                } else if (threeDpitches.curView === 'LHH' || threeDpitches.curView === 'RHH') {
                                    threeDpitches.guidMap1[guid].material.opacity = 0.1;
                                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                                } else {
                                    threeDpitches.guidMap1[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                                }
                            }
                        });
                    });
                }
                break;
            case '4':
                for (let i = 0; i < objects.length; i++) {
                    pitchOutcome = objects[i].pitchOutcome;
                    objects[i].material.color = new THREE.Color(threeDpitches.pitchOutcomeColor(pitchOutcome));
                    guid = objects[i].guid;
                    threeDpitches.guidMap1[guid].visible = true;
                    threeDpitches.guidMap2[guid].visible = true;
                    threeDpitches.guidMap3[guid].visible = true;
                    threeDpitches.guidMap4[guid].visible = true;
                    if (threeDpitches.curView === 'PIT') {
                        threeDpitches.guidMap1[guid].material.opacity = 0.1;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 0.1;
                    } else if (threeDpitches.curView === 'LHH' || threeDpitches.curView === 'RHH') {
                        threeDpitches.guidMap1[guid].material.opacity = 0.1;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    } else {
                        threeDpitches.guidMap1[guid].material.opacity = 1.0;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    }
                }
                pitchInfoSelectorList.empty();
                $.each(threeDpitches.pitchOutcomeGroups, function (key, value) {
                    let group = value['group'];
                    let color = value['color'];
                    let listItem = $("<li id='" + group + "' style='height:25px;'></li>").appendTo(pitchInfoSelectorList);
                    $("<div style='vertical-align: middle; white-space:nowrap; margin-right: 15px; font-size:12px; display:inline-block; width: 10px; height: 10px; background-color: " + color + ";margin-top:5px; margin-left:5px;'></div><p style='vertical-align: middle; font-size:12px; display:inline-block; margin-top: 0px; margin-bottom: 0px;'>" + group + "</p>").appendTo(listItem);
                    listItem.on('mouseenter', function () {
                        this.style.backgroundColor = '#4ca2cd';
                        this.style.cursor = 'pointer';
                        for (let i = 0; i < objects.length; i++) {
                            let pitchOutcomeGroup = objects[i].pitchOutcomeGroup;
                            if (pitchOutcomeGroup !== this.id) {
                                let guid = objects[i].guid;
                                threeDpitches.guidMap1[guid].visible = false;
                                threeDpitches.guidMap2[guid].visible = false;
                                threeDpitches.guidMap3[guid].visible = false;
                                threeDpitches.guidMap4[guid].visible = false;
                            }
                        }
                    });
                    listItem.on('mouseleave', function () {
                        this.style.cursor = 'auto';
                        this.style.backgroundColor = '#d9d9d9';
                        for (let i = 0; i < objects.length; i++) {
                            let guid = objects[i].guid;
                            threeDpitches.guidMap1[guid].visible = true;
                            threeDpitches.guidMap2[guid].visible = true;
                            threeDpitches.guidMap3[guid].visible = true;
                            threeDpitches.guidMap4[guid].visible = true;
                            if (threeDpitches.curView === 'PIT') {
                                threeDpitches.guidMap1[guid].material.opacity = 0.1;
                                threeDpitches.guidMap2[guid].material.opacity = 1.0;
                                threeDpitches.guidMap3[guid].material.opacity = 1.0;
                                threeDpitches.guidMap4[guid].material.opacity = 0.1;
                            } else if (threeDpitches.curView === 'LHH' || threeDpitches.curView === 'RHH') {
                                threeDpitches.guidMap1[guid].material.opacity = 0.1;
                                threeDpitches.guidMap2[guid].material.opacity = 1.0;
                                threeDpitches.guidMap3[guid].material.opacity = 1.0;
                                threeDpitches.guidMap4[guid].material.opacity = 1.0;
                            } else {
                                threeDpitches.guidMap1[guid].material.opacity = 1.0;
                                threeDpitches.guidMap2[guid].material.opacity = 1.0;
                                threeDpitches.guidMap3[guid].material.opacity = 1.0;
                                threeDpitches.guidMap4[guid].material.opacity = 1.0;
                            }
                        }
                    });
                });
                break;
        }
        await promise;
        if(redraw || isLive) {
            let selectionVal = Number(selection - 1);
            threeDpitches.postRender(isLive, selectionVal);
        }
    },

    postRender: async function(isLive, selectionVal) {
        let containerDiv = $('#containerDiv');
        let promise = new Promise((resolve) => {
            resolve();
        });
        threeDpitches.renderer.render( threeDpitches.scene, threeDpitches.camera );
        if(containerDiv.length){
            containerDiv.show();
        }
        await promise;
        $('#loadingDiv').hide();
        if(isLive){
            $('#pitchInfoSelector').prop('selectedIndex',selectionVal);
        }
    },

    render: async function() {
        let promise = new Promise((resolve) => {
            resolve();
        });
        // find intersections
        threeDpitches.raycaster.setFromCamera(threeDpitches.mouse, threeDpitches.camera);
        threeDpitches.objects = threeDpitches.pitchesGroup.children;
        threeDpitches.intersects = threeDpitches.raycaster.intersectObjects(threeDpitches.objects);
        let object, guid, colorHex;

        if (threeDpitches.intersects.length > 0) {
            if (threeDpitches.INTERSECTED !== threeDpitches.intersects[0].object) {
                if (threeDpitches.INTERSECTED) {
                    guid = threeDpitches.INTERSECTED.guid;
                    colorHex = threeDpitches.INTERSECTED.material.emissive.getHex();
                    if (threeDpitches.curView === 'PIT') {
                        threeDpitches.guidMap1[guid].material.opacity = 0.1;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 0.1;
                        threeDpitches.guidMap1[guid].material.transparent = true;
                        threeDpitches.guidMap4[guid].material.transparent = true;
                    } else if (threeDpitches.curView === 'LHH' || threeDpitches.curView === 'RHH') {
                        threeDpitches.guidMap1[guid].material.opacity = 1.0;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 1.0;
                        threeDpitches.guidMap1[guid].material.transparent = false;
                        threeDpitches.guidMap1[guid].material.emissive.setHex(colorHex);
                        threeDpitches.guidMap4[guid].material.transparent = false;
                    } else {
                        threeDpitches.guidMap1[guid].material.opacity = 1.0;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 1.0;
                        threeDpitches.guidMap1[guid].material.transparent = false;
                        threeDpitches.guidMap4[guid].material.transparent = false;
                    }
                    threeDpitches.guidMap2[guid].material.transparent = false;
                    threeDpitches.guidMap3[guid].material.transparent = false;
                }
                threeDpitches.INTERSECTED = threeDpitches.intersects[0].object;
                guid = threeDpitches.INTERSECTED.guid;
                colorHex = threeDpitches.INTERSECTED.material.emissive.getHex();
                if (threeDpitches.curView === 'PIT') {
                    threeDpitches.guidMap1[guid].material.opacity = 0.1;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].material.opacity = 0.1;
                    threeDpitches.guidMap1[guid].material.transparent = true;
                    threeDpitches.guidMap4[guid].material.transparent = true;
                } else if (threeDpitches.curView === 'LHH' || threeDpitches.curView === 'RHH') {
                    threeDpitches.guidMap1[guid].material.opacity = 1.0;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    threeDpitches.guidMap1[guid].material.transparent = false;
                    threeDpitches.guidMap1[guid].material.emissive.setHex(colorHex);
                    threeDpitches.guidMap4[guid].material.transparent = false;
                } else {
                    threeDpitches.guidMap1[guid].material.opacity = 1.0;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    threeDpitches.guidMap1[guid].material.transparent = false;
                    threeDpitches.guidMap4[guid].material.transparent = false;
                }
                threeDpitches.guidMap2[guid].material.transparent = false;
                threeDpitches.guidMap3[guid].material.transparent = false;
            } else {
                for (let i = 0; i < threeDpitches.objects.length; i++) {
                    object = threeDpitches.objects[i];
                    guid = object.guid;
                    if (object !== threeDpitches.INTERSECTED) {
                        threeDpitches.guidMap1[guid].material.opacity = 0.1;
                        threeDpitches.guidMap2[guid].material.opacity = 0.1;
                        threeDpitches.guidMap3[guid].material.opacity = 0.1;
                        threeDpitches.guidMap4[guid].material.opacity = 0.1;
                        threeDpitches.guidMap1[guid].material.transparent = true;
                        threeDpitches.guidMap2[guid].material.transparent = true;
                        threeDpitches.guidMap3[guid].material.transparent = true;
                        threeDpitches.guidMap4[guid].material.transparent = true;
                    }
                }
                guid = threeDpitches.INTERSECTED.guid;
                colorHex = threeDpitches.INTERSECTED.material.emissive.getHex();
                if (threeDpitches.curView === 'PIT') {
                    threeDpitches.guidMap1[guid].material.opacity = 0.1;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].material.opacity = 0.1;
                    threeDpitches.guidMap1[guid].material.transparent = true;
                    threeDpitches.guidMap4[guid].material.transparent = true;
                } else if (threeDpitches.curView === 'LHH' || threeDpitches.curView === 'RHH') {
                    threeDpitches.guidMap1[guid].material.opacity = 1.0;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    threeDpitches.guidMap1[guid].material.transparent = false;
                    threeDpitches.guidMap1[guid].material.emissive.setHex(colorHex);
                    threeDpitches.guidMap4[guid].material.transparent = false;
                } else {
                    threeDpitches.guidMap1[guid].material.opacity = 1.0;
                    threeDpitches.guidMap2[guid].material.opacity = 1.0;
                    threeDpitches.guidMap3[guid].material.opacity = 1.0;
                    threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    threeDpitches.guidMap1[guid].material.transparent = false;
                    threeDpitches.guidMap4[guid].material.transparent = false;
                }
                threeDpitches.guidMap2[guid].material.transparent = false;
                threeDpitches.guidMap3[guid].material.transparent = false;
                $("#pitchInfo").css("visibility", "visible");
                threeDpitches.INTERSECTED.callback();
            }
        } else {
            if (threeDpitches.INTERSECTED) {
                for (let i = 0; i < threeDpitches.objects.length; i++) {
                    object = threeDpitches.objects[i];
                    guid = object.guid;
                    if (threeDpitches.curView === 'PIT') {
                        threeDpitches.guidMap1[guid].material.opacity = 0.1;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 0.1;
                    } else if (threeDpitches.curView === 'LHH' || threeDpitches.curView === 'RHH') {
                        threeDpitches.guidMap1[guid].material.opacity = 0.1;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    } else {
                        threeDpitches.guidMap1[guid].material.opacity = 1.0;
                        threeDpitches.guidMap2[guid].material.opacity = 1.0;
                        threeDpitches.guidMap3[guid].material.opacity = 1.0;
                        threeDpitches.guidMap4[guid].material.opacity = 1.0;
                    }
                    threeDpitches.guidMap1[guid].material.transparent = true;
                    threeDpitches.guidMap2[guid].material.transparent = true;
                    threeDpitches.guidMap3[guid].material.transparent = true;
                    threeDpitches.guidMap4[guid].material.transparent = true;
                }
                let pitchTypeDiv = $('#pitchTypeData');
                pitchTypeDiv.text(" ");
                let pitchSpeedDiv = $('#pitchSpeedData');
                pitchSpeedDiv.text(" ");
                $("#pitchInfo").css("visibility", "hidden");
                threeDpitches.disposeTempStrikeZone();
            }
            threeDpitches.INTERSECTED = null;
        }
        threeDpitches.renderer.render(threeDpitches.scene, threeDpitches.camera);
        await promise;
        $('#loadingDiv').hide();
    },

    disposeTempStrikeZone: function(){
        if(threeDpitches.tempSZAdded) {
            threeDpitches.strikeZoneTempGroup.remove(threeDpitches.szTempLeftMesh);
            threeDpitches.szTempLeftMesh.geometry.dispose();
            threeDpitches.cleanMaterial(threeDpitches.szTempLeftMesh.material);
            threeDpitches.strikeZoneTempGroup.remove(threeDpitches.szTempBottomMesh);
            threeDpitches.szTempBottomMesh.geometry.dispose();
            threeDpitches.cleanMaterial(threeDpitches.szTempBottomMesh.material);
            threeDpitches.strikeZoneTempGroup.remove(threeDpitches.szTempRightMesh);
            threeDpitches.szTempRightMesh.geometry.dispose();
            threeDpitches.cleanMaterial(threeDpitches.szTempRightMesh.material);
            threeDpitches.strikeZoneTempGroup.remove(threeDpitches.szTempTopMesh);
            threeDpitches.szTempTopMesh.geometry.dispose();
            threeDpitches.cleanMaterial(threeDpitches.szTempTopMesh.material);
            let strikeZoneObjects = threeDpitches.strikeZoneGroup.children;
            for (let k = 0; k < strikeZoneObjects.length; k++) {
                strikeZoneObjects[k].material.visible = true;
            }
            threeDpitches.tempSZAdded = false;
        }
    },

    disposeStrikeZone(){
        for (let a = threeDpitches.strikeZoneGroup.children.length - 1; a >= 0; a--) {
            let tempStrikeZoneObj = threeDpitches.strikeZoneGroup.children[a];
            threeDpitches.strikeZoneGroup.remove(tempStrikeZoneObj);
            tempStrikeZoneObj.geometry.dispose();
            threeDpitches.cleanMaterial(tempStrikeZoneObj.material);
        }
    },

    disposePitches: function(){
        for (let k = threeDpitches.pitchesGroup.children.length - 1; k >= 0; k--) {
            let tempPitchObj = threeDpitches.pitchesGroup.children[k];
            threeDpitches.pitchesGroup.remove(tempPitchObj);
            tempPitchObj.geometry.dispose();
            threeDpitches.cleanMaterial(tempPitchObj.material);
        }
    },

    removeGrids: async function(){
        let removingComplete = new Promise((resolve) => {
            resolve();
        });
        for (let a = threeDpitches.gridGroup.children.length - 1; a >= 0; a--) {
            let tempGridObj = threeDpitches.gridGroup.children[a];
            threeDpitches.gridGroup.remove(tempGridObj);
        }
        await(removingComplete);
        if(!threeDpitches.animating) {
            threeDpitches.animating = true;
            threeDpitches.startAnimation();
        }
        if (threeDpitches.timeout !== undefined) {
            window.clearTimeout(threeDpitches.timeout);
        }
        threeDpitches.timeout = window.setTimeout(function () {
            threeDpitches.stopAnimation();
        }, 300);
    },

    reAddGrids: async function(){
        let addingComplete = new Promise((resolve) => {
            resolve();
        });
        threeDpitches.gridGroup
            .add(threeDpitches.gridStorage.axes)
            .add(threeDpitches.gridStorage.xyPlanes[0])
            .add(threeDpitches.gridStorage.xyPlanes[1])
            .add(threeDpitches.gridStorage.xzPlanes[0])
            .add(threeDpitches.gridStorage.xzPlanes[1])
            .add(threeDpitches.gridStorage.yzPlanes[0])
            .add(threeDpitches.gridStorage.yzPlanes[1]);
        await(addingComplete);
        if(!threeDpitches.animating) {
            threeDpitches.animating = true;
            threeDpitches.startAnimation();
        }
        if (threeDpitches.timeout !== undefined) {
            window.clearTimeout(threeDpitches.timeout);
        }
        threeDpitches.timeout = window.setTimeout(function () {
            threeDpitches.stopAnimation();
        }, 300);
    },

    disposeGrids(){
        for (let a = threeDpitches.gridGroup.children.length - 1; a >= 0; a--) {
            let tempGridObj = threeDpitches.gridGroup.children[a];
            threeDpitches.gridGroup.remove(tempGridObj);
            tempGridObj.geometry.dispose();
            threeDpitches.cleanMaterial(tempGridObj.material);
        }
    },

    cleanUp: async function() {
        let cleaningComplete = new Promise((resolve) => {
            resolve();
        });
        threeDpitches.renderer.dispose();
        threeDpitches.disposeGrids();

        threeDpitches.scene.traverse(object => {
            if (!object.isMesh) return;

            object.geometry.dispose();

            if (object.material.isMaterial) {
                threeDpitches.cleanMaterial(object.material)
            } else {
                // array of materials
                for (const material of object.material) threeDpitches.cleanMaterial(material)
            }
        });
        await(cleaningComplete);
        if(!threeDpitches.initialClean){
            cancelAnimationFrame(threeDpitches.animationFrame);
            threeDpitches.animating = false;
        }
        threeDpitches.initialClean = false;
    },

    cleanMaterial: function(material){
        material.dispose();

        // dispose textures
        for (const key of Object.keys(material)) {
            const value = material[key];
            if (value && typeof value === 'object' && 'minFilter' in value) {
                value.dispose()
            }
        }
    }
};