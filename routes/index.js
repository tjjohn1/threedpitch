var express = require('express');
var router = express.Router();
var path = require('path');

/* GET home page. */


router.get('/css/perfect-scrollbar.css', function(req, res, next) {
    res.sendFile('perfect-scrollbar.css', { root: './public/css' });
});

router.get('/css/onitUpdating.css', function(req, res, next) {
    res.sendFile('onitUpdating.css', { root: './public/css' });
});

module.exports = router;
