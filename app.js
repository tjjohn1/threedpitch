var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var opn = require('opn');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();
port = process.env.PORT || 3030;

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/assets', express.static(path.join(__dirname+'/assets')));

app.get('/', function (req, res) { res.sendFile(path.join(__dirname+'/assets/3DPitches.html')); });

module.exports = app;

app.listen(port);

console.log('RESTful API server started on: ' + port);
console.log('Opening chrome browser to ONIT updater tool');

opn('http://localhost:3030', {app: ['chrome']});